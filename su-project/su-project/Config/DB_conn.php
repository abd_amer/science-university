<?php


class DB_conn
{
    public $servername = "localhost";
    public $username = "root";
    public $password = "root";
    public $connection;
    public function connect()
    {
        try {
            $connection = new PDO("mysql:host=$this->servername;dbname=mydb", $this->username, $this->password);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $connection;
        } catch (PDOException $e) {
            echo "Connection failed : " . $e->getMessage();
        }
    }
}

