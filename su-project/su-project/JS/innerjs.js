$(document).ready(function () {


});


$(window).on('load resize orientationchange', function () {
    if ($(window).width() >= 992) {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 120) {
                $(".menu2menu2").addClass("fixed");
            } else {
                $(".menu2").removeClass("fixed");
            }
        });
    } else {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 1) {
                $(".menu2").addClass("fixed");
            } else {
                $(".menu2").removeClass("fixed");
            }
        });
    }
});

$(window).on('load resize orientationchange', function () {
    if ($(window).width() >= 992) {
        $(window).ready(function () {
            if ($(this).scrollTop() > 120) {
                $(".menu2").addClass("fixed");
            } else {
                $(".menu2").removeClass("fixed");
            }
        });
    } else {
        $(window).ready(function () {
            if ($(this).scrollTop() > 1) {
                $(".menu2").addClass("fixed");
            } else {
                $(".menu2").removeClass("fixed");
            }
        });
    }
});


const tel = document.getElementById('tel');
if (tel) {
    tel.addEventListener("input", function () {
        let start = this.selectionStart;
        let end = this.selectionEnd;

        const current = this.value;
        const corrected = current.replace(/([^+0-9]+)/gi, '');
        this.value = corrected;

        if (corrected.length < current.length) --end;
        this.setSelectionRange(start, end);
    });
}

