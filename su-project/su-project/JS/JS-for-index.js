
function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}
var a = 0;
$(window).scroll(function () {

    var oTop = $('#counter').offset().top - window.innerHeight;
    if (a == 0 && $(window).scrollTop() > oTop) {
        $('.counter-value').each(function () {
            var $this = $(this),
                countTo = $this.attr('data-count');
            $({
                countNum: $this.text()
            }).animate({
                    countNum: countTo
                },
                {

                    duration: 2000,
                    easing: 'swing',
                    step: function () {
                        $this.text(commaSeparateNumber(Math.floor(this.countNum)));
                    },
                    complete: function () {
                        $this.text(commaSeparateNumber(this.countNum));
                    }

                });
        });
        a = 1;
    }
});
$(window).ready(function () {

    var oTop = $('#counter').offset().top - window.innerHeight;
    if (a == 0 && $(window).scrollTop() > oTop) {
        $('.counter-value').each(function () {
            var $this = $(this),
                countTo = $this.attr('data-count');
            $({
                countNum: $this.text()
            }).animate({
                    countNum: countTo
                },
                {

                    duration: 2000,
                    easing: 'swing',
                    step: function () {
                        $this.text(commaSeparateNumber(Math.floor(this.countNum)));
                    },
                    complete: function () {
                        $this.text(commaSeparateNumber(this.countNum));
                    }
                });
        });
        a = 1;
    }
});


$(document).ready(function () {



    const tel = document.getElementById('tel');

    tel.addEventListener('input', function () {
        let start = this.selectionStart;
        let end = this.selectionEnd;

        const current = this.value;
        const corrected = current.replace(/([^+0-9]+)/gi, '');
        this.value = corrected;

        if (corrected.length < current.length) --end;
        this.setSelectionRange(start, end);
    });
});


$(window).on('load resize orientationchange', function () {
    if ($(window).width() >= 992) {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 120) {
                $(".menu2").addClass("fixed");
            } else {
                $(".menu2").removeClass("fixed");
            }
        });
    } else {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 1) {
                $(".menu2").addClass("fixed");
            } else {
                $(".menu2").removeClass("fixed");
            }
        });
    }
});

$(window).on('load resize orientationchange', function () {
    if ($(window).width() >= 992) {
        $(window).ready(function () {
            if ($(this).scrollTop() > 120) {
                $(".menu2").addClass("fixed");
            } else {
                $(".menu2").removeClass("fixed");
            }
        });
    } else {
        $(window).ready(function () {
            if ($(this).scrollTop() > 1) {
                $(".menu2").addClass("fixed");
            } else {
                $(".menu2").removeClass("fixed");
            }
        });
    }
});









