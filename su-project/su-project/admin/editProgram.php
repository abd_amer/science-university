<?php
session_start();
require_once "../Global-Variables/global.php";
require_once $config;
require_once "../classes/programs/programs.php";


//session check
if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
}
//get data for selected program
if (isset($_REQUEST['id'])) {
    $uid = $_REQUEST['id'];
    $program = new programs();
    $programResult = $program->editProgram($uid);
    $imgProgramSrc = $program->retrieve_Img($programResult['programImage']);
    $pageResult = $program->editPage($programResult['programPageID']);
    $imgSrc = $program->retrieve_Img($pageResult['genric_pageHeaderImage']);
    $parentID = $program->editProgram($programResult['programParentID']);
    $resultParent = $program->editPage($parentID['programPageID']);
}
$checkOnUpdate = false;
//update selected program
if (isset($_POST['submit'])) {
    $program = new programs();
    $programResult = $program->editProgram($uid);
    $imgProgramSrc = $program->retrieve_Img($programResult['programImage']);
    $pageResult = $program->editPage($programResult['programPageID']);
    $imgSrc = $program->retrieve_Img($pageResult['genric_pageHeaderImage']);
    $target_dir = $fileDirectory;
    $headerPhotoFile = $target_dir . basename($_FILES["headerPhoto"]["name"]);
    $programPhotoFile = $target_dir . basename($_FILES["programPhoto"]["name"]);
    $headerPhoto = basename($_FILES['headerPhoto']['name']);
    $programPhoto = basename($_FILES['programPhoto']['name']);
    if ($headerPhoto == null) {
        $headerPhoto = $imgSrc['imageSrc'];
    }
    if ($programPhoto == null) {
        $programPhoto = $imgProgramSrc['imageSrc'];
    }
    $uploadOk = 1;

// Check if file already exists
    if (file_exists($headerPhotoFile) && file_exists($programPhotoFile)) {
        $uploadOk = 0;
    }
// Check file size
    if ($_FILES["headerPhoto"]["size"] > 500000 && $_FILES["programPhoto"]["size"] > 500000) {
        $uploadOk = 0;
    }

// Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["headerPhoto"]["tmp_name"], $headerPhotoFile) &&
            move_uploaded_file($_FILES["programPhoto"]["tmp_name"], $programPhotoFile)) {
        } else {
        }
    }
    $program = new programs();
    $title = $_POST['title'];
    $body = $_POST['body'];
    $alt = $_POST['headerAlt'];
    $alt2 = $_POST['programAlt'];
    $imgID = $program->saveImage($alt, $headerPhoto);
    $fields = ['genric_pageTitle' => $title, 'genric_pageBody' => $body, 'genric_pageHeaderImage' => $imgID];
    $imgID2 = $program->saveImage($alt2, $programPhoto);
    $programID = $_GET['id'];
    $programResult = $program->editProgram($uid);
    $pageID = $programResult['programPageID'];
    if ($program->updateProgram($imgID2, $programID) && $program->update($fields, $pageID)) {
        $checkOnUpdate = true;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/sb-admin.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="style/css/sb-admin.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>
<body id="page-top">
<?php
include 'header.php';
?>
<div id="content-wrapper">
    <div class="container-fluid">
        <!--title (path)-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">News</li>
        </ol>
    </div>
    <div class="wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Edit News</h2>
                    </div>
                    <!--form filled with the selected item data-->
                    <form id="form-program" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="required">Title</label>
                            <input type="text" class="form-control" max="100" name="title" placeholder="Enter Title"
                                   value="<?php echo $pageResult['genric_pageTitle']; ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Body</label>
                            <textarea type="text" class="form-control ckeditor" name="body" placeholder="Enter Title">
                                <?php echo $pageResult['genric_pageBody']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Parent</label>
                            <input type="text" class="form-control" name="parentID"
                                   value="<?php echo $resultParent['genric_pageTitle']; ?>" disabled>
                        </div>
                        <div>
                            <div><label>Current Header Image</label></div>
                            <img src=<?php echo $imagePath . $imgSrc['imageSrc']; ?>>
                        </div>
                        <div class="form-group">
                            <div><label>Choose Header Image</label></div>
                            <input type="file" name="headerPhoto" id="photo" value="">
                            <label>ALT</label>
                            <input type="text" name="headerAlt" value=<?php echo $imgSrc['imageAlt']; ?>>
                        </div>
                        <div>
                            <div><label>Current Card Image</label></div>
                            <img src=<?php echo $imagePath . $imgProgramSrc['imageSrc']; ?>>
                        </div>
                        <div class="form-group">
                            <div><label class="required">Choose Card Image</label></div>
                            <input type="file" name="programPhoto" id="photo">
                            <label class="required">ALT</label>
                            <input type="text" name="programAlt" value="<?php echo $imgProgramSrc['imageAlt']; ?>">
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php';
if ($checkOnUpdate) {
    echo '<script> swal({
      title: "Successfully Updated !",
      buttons: {ok: "OK"},
      })    
     </script>';
}
?>
<script>
    $(".swal-button--ok").click(function () {
        window.location.href = 'programs.php';
    });
</script>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

</body>

</html>
