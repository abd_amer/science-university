<?php
session_start();
require_once "../Global-Variables/global.php";
require_once $config;
require_once "../classes/menu/menuItem.php";


//session check
if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
}
//get data for selected menu item
$checkOnUpdate = false;
if (isset($_REQUEST['id'])) {
    $uid = $_REQUEST['id'];
    $menu = new menuItem();
    $selectedItemResult = $menu->editItem($uid);
    $selectedTypeResult = $menu->editType($selectedItemResult['menu_itemType']);

}
//update selected menu item
if (isset($_POST['submit'])) {
    if (!isset($_POST['linkable'])) {
        $menu_itemIsLinkable = 0;
    } else {
        $menu_itemIsLinkable = $_POST['linkable'];
    }
    $uid = $_REQUEST['id'];
    $menu = new menuItem();
    $menuItemName = $_POST['menuItem'];
    $itemType = $_POST['itemType'];
    $menu_itemLink = $_POST['itemPath'];
    $menu_itemOrder = $_POST['itemOrder'];
    $parentID = $_POST['parentID'];
    if (!isset($_POST['newLine'])) {
        $newLine = 0;
    } else {
        $newLine = $_POST['newLine'];
    }
    $author = $_SESSION['id'];
    $fields = ['menu_itemName' => $menuItemName, 'menu_itemLink' => $menu_itemLink, 'menu_itemOrder' => $menu_itemOrder, 'menu_itemAutherID' => $author];

    $menu->updateParentID($parentID, $uid);
    $menu->menuItemTypeAndNewLine($newLine, $itemType, $uid ,$menu_itemIsLinkable);
    if ($menu->update($fields, $uid)) {
        $checkOnUpdate = true;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/sb-admin.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="style/css/sb-admin.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>

<body id="page-top">
<?php
include 'header.php';
?>
<div id="content-wrapper">
    <!--title (path)-->
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Menu Items</li>
        </ol>
    </div>
    <div class="wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Edit Menu Items</h2>
                    </div>
                    <!--form filled with the selected item data-->
                    <form id="form-menu-item" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="required">Menu Item</label>
                            <input type="text" class="form-control" name="menuItem"
                                   value="<?php echo $selectedItemResult['menu_itemName']; ?>" required>
                        </div>
                        <?php
                        if ($selectedItemResult['menu_itemIsLinkable'] == 1) {
                            $class = 'checked';
                        } else {
                            $class = null;
                        }
                        ?>
                        <div>
                            <div>
                                <label>Linkable ?</label>
                            </div>
                            <input type="checkbox" <?php echo $class ?> name="linkable" value=1 />
                        </div>
                        <div class="form-group"><label>Item Type</label></div>
                        <div class="form-group">
                            <select name="itemType">
                                <?php

                                if ($selectedItemResult['menu_itemType'] == NULL) {
                                    $select_attribute = null;
                                } else {
                                    $select_attribute = 'selected';
                                    echo '<option value=" ' . $selectedTypeResult['menuID'] . '" ' . $select_attribute . ' >' . $selectedTypeResult['menuType'] . '</option>';
                                }

                                ?>
                                <option value="null">Null</option>
                                <?php
                                $type = new menuItem();
                                $typeResults = $type->selectType();
                                if (is_array($typeResults)) {
                                    foreach ($typeResults as $typeResult) {
                                        echo '<option value=' . $typeResult['menuID'] . '>' . $typeResult['menuType'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="required">Item Path</label>
                            <input type="text" class="form-control" name="itemPath"
                                   value="<?php echo $selectedItemResult['menu_itemLink']; ?>" required>
                        </div>
                        <div class="form-group">
                            <label class="required">Order</label>
                            <input type="number" min="1" max="99999999999" class="form-control" name="itemOrder"
                                   value="<?php echo $selectedItemResult['menu_itemOrder']; ?>" required>
                        </div>
                        <?php
                        if ($selectedItemResult['menu_itemNewLine'] == 1) {
                            $class = 'checked';
                        } else {
                            $class = null;
                        }
                        ?>
                        <div>
                            <div>
                                <label>If the item type is footer, do you want it to be in a new line ? </label>
                            </div>
                            <input type="checkbox" <?php echo $class ?> name="newLine" value=1 />
                        </div>

                        <div class="form-group"><label>Parent</label></div>
                        <div class="form-group">
                            <?php $menuItem = new menuItem();
                            $menuItemResult = $menuItem->editItem($selectedItemResult['menu_itemParentID']);
                            ?>
                            <select name="parentID">
                                <?php
                                if ($selectedItemResult['menu_itemParentID'] == NULL) {
                                } else {
                                    $select_attribute = 'selected';
                                    echo '<option value=" ' . $menuItemResult['menu_itemID'] . '" ' . $select_attribute . ' >' . $menuItemResult['menu_itemName'] . '</option>';
                                }

                                ?>
                                <option value="null">Null</option>
                                <?php
                                $itemsResults = $menuItem->read();
                                if (is_array($itemsResults)) {
                                    foreach ($itemsResults as $itemsResult) {
                                        echo '<option value=' . $itemsResult['menu_itemID'] . '>' . $itemsResult['menu_itemName'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php';
if ($checkOnUpdate) {
    echo '<script> swal({
      title: "Successfully Updated !",
      buttons: {ok: "OK"},
      })    
     </script>';
}
?>
<script>
    $(".swal-button--ok").click(function () {
        window.location.href = 'menuItems.php';
    });
</script>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

</body>

</html>
