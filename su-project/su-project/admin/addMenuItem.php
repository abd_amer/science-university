<?php
session_start();
require_once "../Global-Variables/global.php";
require_once $config;
require_once "../classes/menu/menuItem.php";


//session check
if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
}
$checkOfAdd = false;
if (isset($_POST['submit'])) {
    //check the checkboxes
    $menuItem1 = $_POST['menuItem'];
    if (!isset($_POST['linkable'])) {
        $linkable = 0;
    } else {
        $linkable = $_POST['linkable'];
    }
    $itemType = $_POST['itemType'];
    $itemPath = $_POST['itemPath'];
    $author = $_SESSION['id'];
    $itemOrder = $_POST['itemOrder'];
    $parentID = $_POST['parentID'];
    if (!isset($_POST['newLine'])) {
        $newLine = 0;
    } else {
        $newLine = $_POST['newLine'];
    }
    //insert the fore data
    $menuItem = new menuItem();
    if ($menuItem->insert($newLine, $menuItem1, $linkable, $itemType, $itemPath, $author, $itemOrder, $parentID)) {
        $checkOfAdd = true;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/sb-admin.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="style/css/sb-admin.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>

<body id="page-top">
<?php
include 'header.php';
?>
<div id="content-wrapper">
    <!--title (path)-->
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Menu Items</li>
        </ol>
    </div>
    <div class="wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Add Menu Item</h2>
                    </div>
                    <br>
                    <!--form start-->
                    <form id="add-menu-item" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="required">Menu Item</label>
                            <input type="text" class="form-control" name="menuItem" placeholder="Enter Menu Item"
                                   required>
                        </div>
                        <div class="form-group">
                            <label>Linkable?</label>
                            <input type="checkbox" name="linkable" value=1>
                        </div>
                        <div class="form-group">
                            <div>
                                <label>If the item type is footer, do you want it to be in a new line ? </label>
                            </div>
                            <input type="checkbox" name="newLine" value=1>
                        </div>
                        <div class="form-group">
                            <label>Item Type</label>
                        </div>
                        <div class="form-group">
                            <select name="itemType">
                                <option value="null">Null</option>
                                <?php
                                //get all types
                                $itemType = new menuItem();
                                $itemsResults = $itemType->selectType();
                                if (is_array($itemsResults)) {
                                    foreach ($itemsResults as $itemResult) {
                                        echo '<option value=' . $itemResult['menuID'] . '>' . $itemResult['menuType'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="required">Item Path</label>
                            <input type="text" class="form-control" name="itemPath" placeholder="Enter Item Path"
                                   required>
                        </div>
                        <div class="form-group">
                            <label class="required">Order</label>
                            <input type="number" min="1" max="9999999999" class="form-control" name="itemOrder"
                                   placeholder="Enter Order"
                                   required>
                        </div>


                        <div class="form-group"><label>Parent</label></div>
                        <div class="form-group">

                            <select name="parentID">
                                <option value="null">Null</option>
                                <?php
                                //get all items to select a parent
                                $items = $itemType->read();
                                if (is_array($items)) {
                                    foreach ($items as $item) {
                                        echo '<option value=' . $item['menu_itemID'] . '>' . $item['menu_itemName'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>


<?php
include 'footer.php';
if ($checkOfAdd) {
    echo '<script> swal({
      title: "Successfully Added !",
      })    
     </script>';
}
?>
<script>
    $(".swal-button").click(function () {
        window.location.href = 'menuItems.php';
    });
</script>


<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

</body>

</html>
