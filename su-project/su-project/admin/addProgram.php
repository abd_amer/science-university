<?php
session_start();
require_once "../Global-Variables/global.php";
require_once $config;
require_once "../classes/programs/programs.php";


//session check
if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
}
$checkOfAdd = false;
if (isset($_POST['submit'])) {
// save photo
    $target_dir = $fileDirectory;
    $targetFileHeader = $target_dir . basename($_FILES["headerPhoto"]["name"]);
    $targetFileProgram = $target_dir . basename($_FILES["programPhoto"]["name"]);
    $targetHeader = basename($_FILES['headerPhoto']['name']);
    $targetProgram = basename($_FILES['programPhoto']['name']);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($targetFileHeader, PATHINFO_EXTENSION));
    $imageFileType2 = strtolower(pathinfo($targetFileProgram, PATHINFO_EXTENSION));

// Check if file already exists
    if (file_exists($targetFileHeader) && file_exists($targetFileProgram)) {
        $uploadOk = 0;
    }
// Check file size
    if ($_FILES["headerPhoto"]["size"] > 500000 && $_FILES["programPhoto"]["size"] > 500000) {
        $uploadOk = 0;
    }
// Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif") {
        $uploadOk = 0;
    }
    if ($imageFileType2 != "jpg" && $imageFileType2 != "png" && $imageFileType2 != "jpeg"
        && $imageFileType2 != "gif") {
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["headerPhoto"]["tmp_name"], $targetHeader) && move_uploaded_file($_FILES["programPhoto"]["tmp_name"], $targetProgram)) {
//            header('Location: news.php');
        } else {
        }
    }
    $title = $_POST['title'];
    $body = $_POST['body'];
    $headerAlt = $_POST['headerAlt'];
    $programAlt = $_POST['programAlt'];
    $author = $_SESSION['id'];
    $parentID = $_POST['parentID'];
    $program = new programs();
// insert data
    if ($program->insert($title, $body, $author, $headerAlt, $targetHeader, $targetProgram, $programAlt, $parentID)) {
        $checkOfAdd = true;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/sb-admin.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="style/css/sb-admin.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>
<body id="page-top">
<?php
include 'header.php';
?>
<div id="content-wrapper">
    <div class="container-fluid">
        <!--title (path)-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Program</li>
        </ol>
    </div>
    <div class="wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Add Program</h2>
                    </div>
                    <!--form start-->
                    <form id="add-program" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="required">Title</label>
                            <input type="text" class="form-control" max="100" name="title" placeholder="Enter Title"
                                   required>
                        </div>
                        <div class="form-group">
                            <label>Body</label>
                            <textarea type="text" class="form-control ckeditor" name="body"
                                      placeholder="Enter Title"></textarea>
                        </div>
                        <div class="form-group">

                            <select name="parentID">
                                <option value="null">Null</option>
                                <?php
                                //select parent for the program
                                $program = new programs();
                                $programResults = $program->read();
                                if (is_array($programResults)) {
                                    foreach ($programResults as $programResult) {
                                        echo '<option value=' . $programResult['programID'] . '>' . $programResult['genric_pageTitle'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <div>
                                <label>Choose Header Image</label>
                            </div>
                            <input type="file" name="headerPhoto" id="headerPhoto">
                            <label>ALT</label>
                            <input type="text" name="headerAlt">
                        </div>
                        <div class="form-group">
                            <div>
                                <label class="required">Choose Card Image</label>
                            </div>
                            <input type="file" name="programPhoto" id="programPhoto" required>
                            <label class="required">ALT</label>
                            <input type="text" name="programAlt" required>
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                    <!--form end-->
                </div>
            </div>
        </div>
    </div>

</div>


<?php
include 'footer.php';
if ($checkOfAdd) {
    echo '<script> swal({
      title: "Successfully Added !",
      })    
     </script>';
}
?>
<script>
    $(".swal-button").click(function () {
        window.location.href = 'programs.php';
    });
</script>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

</body>

</html>
