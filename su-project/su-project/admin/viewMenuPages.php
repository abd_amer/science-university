<?php
session_start();
require_once "../Global-Variables/global.php";
require_once $config;
require_once "../classes/menuPages/menuPages.php";


// check session
if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
}
//get data of the selected item
if (isset($_REQUEST['id'])) {
    $uid = $_REQUEST['id'];
    $page = new menuPages();
    $pageResult = $page->edit($uid);
    $imgSrc = $page->retrieve_Img($pageResult['genric_pageHeaderImage']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="style/css/sb-admin.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>

</head>

<body id="page-top">
<?php
include 'header.php';
?>
<div id="content-wrapper">
    <div class="container-fluid">
        <!--title (path)-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Menu Pages</li>
        </ol>
    </div>
    <div class="wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">View Menu Page</h2>
                    </div>
                    <!--form start-->
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" name="title"
                                   value="<?php echo $pageResult['genric_pageTitle'] ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label>Body</label>
                            <textarea type="text" class="form-control" name="body" placeholder="Enter Title"
                                      disabled><?php echo strip_tags($pageResult['genric_pageBody']) ?></textarea>
                        </div>
                        <div class="form-group">
                            <div><label>Current Header Image</label></div>
                            <img src=<?php echo $imagePath . $imgSrc['imageSrc']; ?>>
                        </div>
                        <div class="form-group">
                            <label>ALT</label>
                            <input type="text" value="<?php echo $imgSrc['imageAlt'] ?>" name="alt" disabled>
                        </div>
                        <a href='menuPages.php?'><span class='btn btn-primary'>Back</span></a>
                    </form>
                    <!--form end-->
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php';
?>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>

<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

</body>
</html>
