<?php
session_start();
require_once "../Global-Variables/global.php";
require_once $config;
require_once "../classes/menu/menuItem.php";


// check session
if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
}
//get data of the selected item
if (isset($_REQUEST['id'])) {
    $uid = $_REQUEST['id'];
    $menu = new menuItem();
    $menuResult = $menu->editItem($uid);
    $menuTypeResult = $menu->editType($menuResult['menu_itemType']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="style/css/sb-admin.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>
<body id="page-top">
<?php
include 'header.php';
?>
<div id="content-wrapper">
    <div class="container-fluid">
        <!--title (path)-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Menu Items</li>
        </ol>
    </div>
    <div class="wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">View Menu Items</h2>
                    </div>
                    <!--form start-->
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Menu Item</label>
                            <input type="text" class="form-control" name="menuItem"
                                   value="<?php echo $menuResult['menu_itemName']; ?>" disabled>
                        </div>
                        <?php
                        if ($menuResult['menu_itemIsLinkable'] == 1) {
                            $class = 'checked';
                        } else {
                            $class = null;
                        }
                        ?>
                        <div>
                            <div>
                                <label>Linkable ?</label>
                            </div>
                            <input type="checkbox" <?php echo $class ?> name="linkable" value=1 disabled />
                        </div>
                        <div class="form-group"><label>Item Type</label></div>
                        <div class="form-group">
                            <input type="text" class="form-control" min="0" max="1" name="linkable"
                                   value="<?php echo $menuTypeResult['menuType']; ?>" disabled>
                        </div>
                        <div class="form-group">
                            <div>
                                <label>If the item type is footer, do you want it to be in a new line ? </label>
                            </div>
                            <input type="checkbox" name="newLine" value=1 disabled/>
                        </div>
                        <div class="form-group">
                            <label>Item Path</label>
                            <input type="text" class="form-control" name="itemPath"
                                   value="<?php echo $menuResult['menu_itemLink']; ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label>Order</label>
                            <input type="number" min="1" class="form-control" name="itemOrder"
                                   value="<?php echo $menuResult['menu_itemOrder']; ?>" disabled>
                        </div>
                        <?php $menu = new menuItem();
                        $menuParent = $menu->editItem($menuResult['menu_itemParentID']);
                        ?>
                        <div class="form-group"><label>Parent</label></div>
                        <div class="form-group">
                            <input type="text" class="form-control" min="0" max="1" name="linkable"
                                   value="<?php echo $menuParent['menu_itemName']; ?>" disabled>
                        </div>
                        <div class="form-group">
                        </div>
                        <a href='menuItems.php?'><span class='btn btn-primary'>Back</span></a>
                    </form>
                    <!--form start-->
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php';
?>
<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>

<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

</body>
</html>
