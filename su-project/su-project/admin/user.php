<?php
session_start();
require_once "../Global-Variables/global.php";
require_once $config;
require_once "../classes/user/user.php";


// check session
if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
}
$checkOnDelete = false;
//delete record
if (isset($_GET['del'])) {
    $id = $_GET['del'];
    $user = new user();
    if ($user->delete($id)) {
        $checkOnDelete = true;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="style/css/sb-admin.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<body id="page-top">
<?php
include 'header.php';
?>
<div id="content-wrapper">
    <div class="container-fluid">
        <!--title (path)-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">User</li>
        </ol>
    </div>
    <div class="wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">User Details</h2>
                        <a href="addUser.php" class="btn btn-success pull-right">Add New User</a>
                    </div>
                    <br>
                    <?php
                    //get all users

                    $user = new user();
                    $userResults = $user->read();
                    echo "<table class='table table-bordered table-striped'>";
                    echo "<thead>";
                    echo "<tr>";
                    echo "<th>#</th>";
                    echo "<th>User Name</th>";
                    echo "<th>Role</th>";
                    echo "<th>Full Name</th>";
                    echo "<th>Action</th>";
                    echo "</tr>";
                    echo "</thead>";
                    echo "<tbody>";
                    if (is_array($userResults)) {
                        foreach ($userResults as $userResult) {
                            if ($userResult['userRole'] == 1) {
                                $role = 'Admin';
                            } else {
                                $role = 'User';
                            }
                            echo "<tr>";
                            echo "<td>" . $userResult['userID'] . "</td>";
                            echo "<td>" . $userResult['userName'] . "</td>";
                            echo "<td>    $role </td>";
                            echo "<td>" . $userResult['userFullName'] . "</td>";
                            echo "<td>";
                            echo "<a href='user.php?del=" . $userResult['userID'] . "' title='Delete Record'><span class='btn btn-danger'>Delete</span></a>";
                            echo "</td>";
                            echo "</tr>";
                        }
                    }
                    echo "</tbody>";
                    echo "</table>";
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php';
if ($checkOnDelete) {
    echo '<script> swal({
      title: "Successfully Deleted !",
      buttons: {ok: "OK"},
      })    
     </script>';
}
?>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>

<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

</body>
</html>
