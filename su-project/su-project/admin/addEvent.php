<?php
session_start();
require_once "../Global-Variables/global.php";
require_once $config;
require_once "../classes/events/events.php";


//session check
if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
}
$invalidTime = false;
$checkOfAdd = false;
if (isset($_POST['submit'])) {
// save photo into specific directory
    // save photo
    $target_dir = $fileDirectory;
    $targetFileHeader = $target_dir . basename($_FILES["headerPhoto"]["name"]);
    $targetFileEvent = $target_dir . basename($_FILES["eventPhoto"]["name"]);
    $targetHeader = basename($_FILES['headerPhoto']['name']);
    $targetEvent = basename($_FILES['eventPhoto']['name']);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($targetFileHeader, PATHINFO_EXTENSION));
    $imageFileType2 = strtolower(pathinfo($targetFileEvent, PATHINFO_EXTENSION));

// Check if file already exists
    if (file_exists($targetFileHeader) && file_exists($targetFileEvent)) {
        $uploadOk = 0;
    }
// Check file size
    if ($_FILES["headerPhoto"]["size"] > 500000 && $_FILES["eventPhoto"]["size"] > 500000) {
        $uploadOk = 0;
    }
// Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif") {
        $uploadOk = 0;
    }
    if ($imageFileType2 != "jpg" && $imageFileType2 != "png" && $imageFileType2 != "jpeg"
        && $imageFileType2 != "gif") {
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["headerPhoto"]["tmp_name"], $targetHeader) && move_uploaded_file($_FILES["eventPhoto"]["tmp_name"], $targetEvent)) {
//            header('Location: news.php');
        } else {
        }
    }

// get data and insert them
    $date = $_POST['date'];
    $campus = $_POST['campus'];
    $title = $_POST['title'];
    $body = $_POST['body'];
    $headerAlt = $_POST['headerAlt'];
    $eventAlt = $_POST['eventAlt'];
    $author = $_SESSION['id'];
    $startTime = $_POST['startTime'];
    $endTime = $_POST['endTime'];
    if ($endTime > $startTime) {
        $event = new events();
        if ($event->insert($date, $title, $body, $author, $headerAlt, $targetHeader, $targetEvent, $eventAlt, $startTime, $endTime, $campus)) {
            $checkOfAdd = true;
        }
    } else {
        $invalidTime = true;
    }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/sb-admin.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!--    <script src="js/sb-admin.js"></script>-->
    <!-- Custom styles for this template-->
    <link href="style/css/sb-admin.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>


</head>

<body id="page-top">
<?php
include 'header.php';
?>
<div id="content-wrapper">
    <!--title (path)-->
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Event</li>
        </ol>
    </div>
    <div class="wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix header">
                        <h2 class="pull-left">Add Event</h2>
                    </div>
                    <!--form start-->
                    <form id="add-event" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="required">Date</label>
                            <input type="date" id="date" name="date" class="form-control"
                                   required>
                        </div>
                        <div class="form-group">
                            <label class="required">Start Time</label>
                            <input type="time" id="date" name="startTime" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="required">End Time</label>
                            <input type="time" id="date" name="endTime" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="required">Title</label>
                            <input type="text" class="form-control" name="title" max="100" placeholder="Enter Title"
                                   required>
                        </div>
                        <div class="form-group">
                            <label class="required">Campus</label>
                            <input type="text" class="form-control" name="campus" placeholder="Enter Campus" required>
                        </div>

                        <div class="form-group">
                            <label>Body</label>
                            <textarea type="text" class="form-control ckeditor" name="body"
                                      placeholder="Enter Title"></textarea>
                        </div>
                        <div class="form-group">
                            <div>
                                <label>Header Image</label>
                            </div>
                            <input type="file" name="headerPhoto">
                            <label>ALT</label>
                            <input type="text" name="headerAlt">
                        </div>
                        <div class="form-group">
                            <div>
                                <label class="required">Event Image</label>
                            </div>
                            <input type="file" name="eventPhoto" id="eventPhoto" required>
                            <label class="required">ALT</label>
                            <input type="text" name="eventAlt" required>
                        </div>
                        <button type="submit" id="news" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                    <!--form end-->
                </div>
            </div>
        </div>
    </div>

</div>


<?php
include 'footer.php';
if ($checkOfAdd) {
    echo '<script> swal({
      title: "Successfully Added !",
      })    
     </script>';

}
if ($invalidTime) {
    echo '<script> swal({
      title: "Invalid Time !",
      })    
     </script>';
}
?>
<?php
if ($checkOfAdd) {
    ?>
    <script>
        $(".swal-button").click(function () {
            window.location.href = 'events.php';
        });
    </script>
    <?php
}
?>
<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

</body>

</html>
