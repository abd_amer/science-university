<?php
session_start();
require_once "../Global-Variables/global.php";
require_once $config;
require_once "../classes/events/events.php";


//session check
if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
}
$checkOnUpdate = false;
$invalidTime = false;
//get data for selected event
if (isset($_REQUEST['id'])) {
    $uid = $_REQUEST['id'];
    $event = new events();
    $eventResult = $event->editEvent($uid);
    $imgEventSrc = $event->retrieve_Img($eventResult['eventImage']);
    $pageResult = $event->editPage($eventResult['eventPageID']);
    $headerImg = $event->retrieve_Img($pageResult['genric_pageHeaderImage']);
    $eventImageSource = $imgEventSrc['imageSrc'];
    $eventImageAlternative = $imgEventSrc['imageAlt'];
    $headerImgSource = $headerImg['imageSrc'];
    $headerImgAlternative = $headerImg['imageAlt'];
}
//update selected event
if (isset($_POST['submit'])) {
    $event = new events();
    $uid = $_REQUEST['id'];
    $eventResult = $event->editEvent($uid);
    $imgEventSrc = $event->retrieve_Img($eventResult['eventImage']);
    $pageResult = $event->editPage($eventResult['eventPageID']);
    $headerImg = $event->retrieve_Img($pageResult['genric_pageHeaderImage']);
    $target_dir = $fileDirectory;
    $targetHeaderPhoto = $target_dir . basename($_FILES["headerPhoto"]["name"]);
    $targetEventPhoto = $target_dir . basename($_FILES["eventPhoto"]["name"]);
    $headerPhoto = basename($_FILES['headerPhoto']['name']);
    $eventPhoto = basename($_FILES['eventPhoto']['name']);
    //if no image selected on update the image value will be the old one
    if ($headerPhoto == null) {
        $headerPhoto = $headerImg['imageSrc'];
    }
    if ($eventPhoto == null) {
        $eventPhoto = $imgEventSrc['imageSrc'];
    }
    $uploadOk = 1;

// Check if file already exists
    if (file_exists($targetHeaderPhoto) && file_exists($targetEventPhoto)) {
        $uploadOk = 0;
    }
// Check file size
    if ($_FILES["headerPhoto"]["size"] > 500000 && $_FILES["eventPhoto"]["size"] > 500000) {
        $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["headerPhoto"]["tmp_name"], $targetHeaderPhoto) &&
            move_uploaded_file($_FILES["eventPhoto"]["tmp_name"], $targetEventPhoto)) {
        } else {
        }
    }
    $event = new events();
    $date = $_POST['date'];
    $title = $_POST['title'];
    $body = $_POST['body'];
    $headerImgAlt = $_POST['headerImgAlt'];
    $eventImgAlt = $_POST['eventImgAlt'];
    $eventCampus = $_POST['campus'];
    $startTime = $_POST['startTime'];
    $endTime = $_POST['endTime'];
    $headerImgID = $event->saveImage($headerImgAlt, $headerPhoto);
    $fields = ['genric_pageTitle' => $title, 'genric_pageBody' => $body, 'genric_pageDate' => $date, 'genric_pageHeaderImage' => $headerImgID];
    $eventImgID = $event->saveImage($eventImgAlt, $eventPhoto);
    $eventFields = ['eventDate' => $date, 'eventImage' => $eventImgID, 'eventStartTime' => $startTime, 'eventEndTime' => $endTime,
        'eventCampus' => $eventCampus];
    $eventID = $_GET['id'];
    $eventResult = $event->editEvent($uid);
    $pageID = $eventResult['eventPageID'];
    if ($endTime > $startTime) {
        if ($event->update($fields, $eventFields, $pageID, $eventID)) {
            $checkOnUpdate = true;
        }
    } else {
        $invalidTime = true;

    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/sb-admin.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="style/css/sb-admin.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="http://code.jquery.com/jquery.js"></script>

    <script src="js/sb-admin.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>
<body id="page-top">
<?php
include 'header.php';
?>
<div id="content-wrapper">
    <div class="container-fluid">
        <!--title (path)-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Events</li>
        </ol>
    </div>
    <div class="wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Edit Event</h2>
                    </div>
                    <!--form filled with the selected item data-->
                    <form id="form-event" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="required">Date</label>
                            <input type="date" id="date" name="date" value="<?php echo $eventResult['eventDate']; ?>"
                                   class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="required">Start Time</label>
                            <input type="time" id="date" name="startTime" class="form-control"
                                   value="<?php echo $eventResult['eventStartTime']; ?>" required>
                        </div>
                        <div class="form-group">
                            <label class="required">End Time</label>
                            <input type="time" id="date" name="endTime" class="form-control"
                                   value="<?php echo $eventResult['eventEndTime']; ?>" required>
                        </div>
                        <div class="form-group">
                            <label class="required">Title</label>
                            <input type="text" class="form-control" name="title"
                                   value="<?php echo $pageResult['genric_pageTitle']; ?>" placeholder="Enter Title"
                                   max="100"
                                   required>
                        </div>
                        <div class="form-group">
                            <label class="required">Campus</label>
                            <input type="text" class="form-control" name="campus"
                                   value="<?php echo $eventResult['eventCampus']; ?>"
                                   placeholder="Enter Campus" required>
                        </div>

                        <div class="form-group">
                            <label>Body</label>
                            <textarea type="text" class="form-control ckeditor" name="body" placeholder="Enter Title">
                                <?php echo $pageResult['genric_pageBody']; ?></textarea>
                        </div>
                        <div>
                            <div><label>Current Header Image</label></div>
                            <img src=<?php echo $imagePath . $headerImgSource; ?>>
                        </div>
                        <div class="form-group">
                            <div><label>Choose Header Image</label></div>
                            <input type="file" name="headerPhoto" id="photo" value="">
                            <label>ALT</label>
                            <input type="text" name="headerImgAlt" value=<?php echo $headerImgAlternative; ?>>
                        </div>
                        <div>
                            <div><label>Current Event Image</label></div>
                            <img src=<?php echo $imagePath . $eventImageSource; ?>>
                        </div>
                        <div class="form-group">
                            <div>
                                <label>Choose Event Image</label>
                            </div>
                            <input type="file" name="eventPhoto" id="photo">
                            <label class="required">ALT</label>
                            <input type="text" name="eventImgAlt" value="<?php echo $eventImageAlternative; ?>"
                                   required>
                        </div>
                        <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>


<?php
include 'footer.php';
if ($checkOnUpdate) {
    echo '<script> swal({
      title: "Successfully Updated !",
      buttons: {ok: "OK"},
      })    
     </script>';
}
if ($invalidTime) {
    echo '<script> swal({
      title: "Invalid Time !",
      })    
     </script>';
}
?>
<?php
if ($checkOnUpdate) {
    ?>
    <script>
        $(".swal-button").click(function () {
            window.location.href = 'events.php';
        });
    </script>
    <?php
}

?>

<script>
    $(".swal-button--ok").click(function () {
        window.location.href = 'events.php';
    });
</script>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

</body>

</html>
