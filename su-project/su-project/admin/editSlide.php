<?php
session_start();
require_once "../Global-Variables/global.php";
require_once $config;
require_once "../classes/slide/slide.php";


//session check
if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
}
$invalidOrder = false;
$checkOnUpdate = false;
//get data for selected slide
if (isset($_REQUEST['id'])) {
    $uid = $_REQUEST['id'];
    $slide = new slide();
    $slideResult = $slide->edit($uid);
    $imgSrc = $slide->retrieve_Img($slideResult['slideImg']);
}
$checkOnUpdate = false;
//update selected slide
if (isset($_POST['submit'])) {
    $target_dir = $fileDirectory;
    $target_file = $target_dir . basename($_FILES["photo"]["name"]);
    $target = basename($_FILES['photo']['name']);
    $slide = new slide();
    $slideResult = $slide->edit($uid);
    $imgSrc = $slide->retrieve_Img($slideResult['slideImg']);
    if ($target == null) {
        $target = $imgSrc['imageSrc'];
    }
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

// Check if file already exists
    if (file_exists($target_file)) {
        $uploadOk = 0;
    }
// Check file size
    if ($_FILES["photo"]["size"] > 500000) {
        $uploadOk = 0;
    }
// Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif") {
        $uploadOk = 0;
    }
// Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
        } else {
        }
    }

    $slide = new slide();
    $text = $_POST['text'];
    $order = $_POST['order'];
    $alt = $_POST['alt'];

    $slideResults = $slide->read();
    foreach ($slideResults as $slideResult) {
        if ($order == $slideResult['slideOrder'] && $order != $slideResult['slideOrder']) {
            $invalidOrder = true;
            break;
        } else {
            $invalidOrder = false;
        }
    }
    if (!$invalidOrder) {
        $imgID = $slide->saveImage($alt, $target);
        $fields = ['slideText' => $text, 'slideOrder' => $order, 'slideImg' => $imgID];
        $id = $_GET['id'];
        if ($slide->update($fields, $id)) {
            $checkOnUpdate = true;
        }
    } else {
        $invalidOrder = true;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/sb-admin.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="style/css/sb-admin.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>
<body id="page-top">
<?php
include 'header.php';
?>
<div id="content-wrapper">
    <div class="container-fluid">
        <!--title (path)-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Slide</li>
        </ol>
    </div>
    <div class="wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Edit Slide</h2>
                    </div>
                    <!--form filled with the selected item data-->
                    <form id="form-slide" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="required">Text</label>
                            <input type="text" maxlength="45" name="text"
                                   value="<?php echo $slideResult['slideText']; ?>"
                                   class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="required">Order</label>
                            <input type="number" min="1" max="999" class="form-control" name="order"
                                   value="<?php echo $slideResult['slideOrder']; ?>" placeholder="Enter Title"
                                   required>
                        </div>
                        <div>
                            <div><label>Current Slide Image</label></div>
                            <img src=<?php echo $imagePath . $imgSrc['imageSrc']; ?>>
                        </div>
                        <div class="form-group">
                            <div><label class="required">Choose Slide Image</label></div>
                            <input type="file" name="photo" id="photo">
                            <label class="required">ALT</label>
                            <input type="text" name="alt" value="<?php echo $imgSrc['imageAlt']; ?>">
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php';
if ($checkOnUpdate) {
    echo '<script> swal({
      title: "Successfully Updated !",
      buttons: {ok: "OK"},
      })    
     </script>';
}
if ($invalidOrder) {
    echo '<script> swal({
      title: "Order already exist !",
      })    
     </script>';
}
?>
<?php
if ($checkOnUpdate) {
    ?>
    <script>
        $(".swal-button").click(function () {
            window.location.href = 'slide.php';
        });
    </script>
    <?php
}
?>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

</body>

</html>
