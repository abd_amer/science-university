<?php
include "../Global-Variables/global.php";
include $config;
include '../classes/reed-for-home-page/get.php';
include '../classes/User.php';
$userLogin = new User();
$username = "";
$password = "";
$errmsg = "";
//login check
if (isset($_POST['username']) && $_POST['password']) {
    $username = $_POST["username"];
    $password = $_POST["password"];
    if ($userLogin->login($username, $password)) {
        header('Location: index.php');
    } else {
        $errmsg = "Incorrect Username or Password";
    }
}
?>
<!DOCTYPE html>
<html>
<head>

    <title>Sciences University</title>
    <!-- CSS for the website-->
    <link href="../style/style.css" rel="stylesheet" type="text/css">
    <link href="../style/style-login.css" rel="stylesheet" type="text/css">

    <link href='https://fonts.googleapis.com/css?family=Roboto:500,900,100,300,700,400' rel='stylesheet'
          type='text/css'>

    <!-- Link For Icons-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- bootstrap 4 -->
    <link href="../style/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- JQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery.js"></script>


    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap"
          rel="stylesheet">

</head>
<body>
<header>
    <?php
    include '../header.php';
    ?>
</header>
<div class="main">
    <p class="back"><a href="../Index.php">&#x2039; HOME</a></p>
    <p class="sign" align="center">Sign in</p>
    <form class="form1" action="" method="post">
        <input class="un " type="text" align="center" placeholder="Username" name="username" required>
        <input class="pass" type="password" align="center" placeholder="Password" name="password" required>
        <input class="submit" type="submit" name="login" value="Sign In">
        <?php
        if (isset($_POST['username']) && $_POST['password']) {
            if (!($userLogin->login($username, $password))) {
                echo "<div class='alert alert-danger' style='text-align: center'>" . $errmsg . "</div>";
            }
        }
        ?>
    </form>
</div>
<footer>
    <?php include '../footer.php'; ?>
</footer>
<script src="../JS/innerjs.js"></script>
</body>
</html>