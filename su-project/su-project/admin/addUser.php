<?php
session_start();
require_once "../Global-Variables/global.php";
require_once $config;
require_once "../classes/user/user.php";


//session check
if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
}
$checkOfAdd = false;
$uniqueUser = true;
if (isset($_POST['submit'])) {
    $user = new user();
    $userName = $_POST['userName'];
    $userFullName = $_POST['userFullName'];
    //check the role checkbox
    if (!isset($_POST['userRole'])) {
        $userRole = 0;
    } else {
        $userRole = $_POST['userRole'];
    }
    $userPassword = $_POST['userPassword'];
    $oldUsers = $user->read();
    //check if username is unique
    if (is_array($oldUsers)) {
        foreach ($oldUsers as $oldUser) {
            if ($userName === $oldUser['userName']) {
                $uniqueUser = false;
                break;
            } else {
                $uniqueUser = true;
            }
        }
    }
    if ($uniqueUser) {
        //insert data
        if ($user->register($userName, $userFullName, $userRole, $userPassword)) {
            $checkOfAdd = true;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/sb-admin.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="style/css/sb-admin.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>

<body id="page-top">
<?php
include 'header.php';
?>
<div id="content-wrapper">
    <div class="container-fluid">
        <!--title (path)-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Users</li>
        </ol>
    </div>
    <div class="wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Add User</h2>
                    </div>
                    <!--form start-->
                    <form id="add-user" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="required">User Name</label>
                            <input type="text" class="form-control" name="userName" placeholder="Enter Name" required>
                        </div>
                        <div class="form-group">
                            <label class="required">User Full Name</label>
                            <input type="text" class="form-control" name="userFullName" placeholder="Enter Full Name"
                                   required>
                        </div>
                        <div class="form-group">
                            <div>
                                <label>Admin ?</label>
                            </div>
                            <input type="checkbox" name="userRole" value=1>
                        </div>
                        <div class="form-group">
                            <label class="required">User Password</label>
                            <input type="password" class="form-control" name="userPassword" min="4"
                                   placeholder="Enter Password" required>
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                    <!--form end-->
                </div>
            </div>
        </div>
    </div>

</div>


<?php
include 'footer.php';
//popup when username is exist
if (!$uniqueUser) {
    echo '<script>
    swal({
      title: "User Name Already Exist !",
        buttons: {cancel: "OK"},
      })    
     </script>';
}
if ($checkOfAdd) {
    echo '<script> swal({
      title: "Successfully Added !",
      buttons: {ok: "OK"},
      })    
     </script>';
}
?>
<script>
    $(".swal-button--ok").click(function () {
        window.location.href = 'user.php';
    });
</script>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

</body>

</html>
