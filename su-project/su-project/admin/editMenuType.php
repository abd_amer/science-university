<?php
session_start();
require_once "../Global-Variables/global.php";
require_once $config;
require_once "../classes/menu/menuType.php";
require_once "../classes/menu/menuItem.php";

//session check
if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
}
$checkOnUpdate = false;
//get data for selected menu type
if (isset($_REQUEST['id'])) {
    $uid = $_REQUEST['id'];
    $menu = new menuType();
    $menuResult = $menu->edit($uid);
    $itemsResult = $menu->items($uid);

}
//update selected menu type
if (isset($_POST['submit'])) {
    $menu = new menuType();
    $menuType = $_POST['menuType'];
    $id = $_GET['id'];
    if ($menu->update($menuType, $id)) {
        $checkOnUpdate = true;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/sb-admin.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="style/css/sb-admin.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>
<body id="page-top">
<?php
include 'header.php';
?>
<div id="content-wrapper">
    <div class="container-fluid">
        <!--title (path)-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Menu Types</li>
        </ol>
    </div>
    <div class="wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Edit Menu Type</h2>
                    </div>
                    <!--form filled with the selected item data-->
                    <form id="form-menu-type" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="required">Menu Type Name</label>
                            <input type="text" class="form-control" name="menuType"
                                   value="<?php echo $menuResult['menuType']; ?>" required>
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div class="col-md-12 margin">
                    <?php
                    //get all slides
                    $menu = new menuItem();
                    $menuChildes = $menu->getChildes($uid);
                    echo "<table class='table table-bordered table-striped'>";
                    echo "<thead>";
                    echo "<tr>";
                    echo "<th>Childes</th>";
                    echo "</tr>";
                    echo "</thead>";
                    echo "<tbody>";
                    if (is_array($menuChildes)) {
                        foreach ($menuChildes as $menuChild) {
                            echo "<tr>";
                            echo "<td>" . $menuChild['menu_itemName'] . "</td>";
                            echo "</tr>";
                        }
                    }
                    echo "</tbody>";
                    echo "</table>";
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php';
if ($checkOnUpdate) {
    echo '<script> swal({
      title: "Successfully Updated !",
      buttons: {ok: "OK"},
      })    
     </script>';
}
?>
<script>
    $(".swal-button--ok").click(function () {
        window.location.href = 'menuTypes.php';
    });
</script>


<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

</body>

</html>
