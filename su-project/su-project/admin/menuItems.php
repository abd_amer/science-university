<?php
session_start();
require_once "../Global-Variables/global.php";
require_once $config;
require_once "../classes/menu/menuItem.php";


// check session
if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
}
$checkOnDelete = false;
//delete record
if (isset($_GET['del'])) {
    $id = $_GET['del'];
    $menuItem = new menuItem();
    if ($menuItem->delete($id)) {
        $checkOnDelete = true;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/sb-admin.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="style/css/sb-admin.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<body id="page-top">
<?php
include 'header.php';
?>
<div id="content-wrapper">
    <div class="container-fluid">
        <!--title (path)-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Menu Items</li>
        </ol>
    </div>

    <div class="wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Menu Items Details</h2>
                        <a href="addMenuItem.php" class="btn btn-success pull-right">Add New Menu Items</a>
                    </div>
                    <br>
                    <?php
                    //get all menu items
                    $menuItem = new menuItem();
                    $itemResults = $menuItem->read();
                    echo "<table class='table table-bordered table-striped'>";
                    echo "<thead>";
                    echo "<tr>";
                    echo "<th>#</th>";
                    echo "<th>Menu Item</th>";
                    echo "<th>Item Type</th>";
                    echo "<th>Parent</th>";
                    echo "<th>Linkable?</th>";
                    echo "<th>Action</th>";
                    echo "</tr>";
                    echo "</thead>";
                    echo "<tbody>";
                    if (is_array($itemResults)) {
                        foreach ($itemResults as $itemResult) {
                            if ($itemResult['menu_itemIsLinkable'] == 1) {
                                $linkable = 'Yes';
                            } else {
                                $linkable = 'No';
                            }
                            $type = $menuItem->itemType($itemResult['menu_itemID']);
                            $parent = $menuItem->editItem($itemResult['menu_itemParentID']);
                            echo "<tr>";
                            echo "<td>" . $itemResult['menu_itemID'] . "</td>";
                            echo "<td>" . $itemResult['menu_itemName'] . "</td>";
                            echo "<td>" . $type['menuType'] . "</td>";
                            echo "<td>" . $parent['menu_itemName'] . "</td>";
                            echo "<td>$linkable</td>";
                            echo "<td>";
                            echo "<a href='viewMenuItem.php?id=" . $itemResult['menu_itemID'] . "' title='View Record'><span class='btn btn-primary'>View</span></a>";
                            echo "<a href='editMenuItem.php?id=" . $itemResult['menu_itemID'] . "' title='Update Record'><span class='btn btn-secondary'>Edit</span></a>";
                            echo "<a href='menuItems.php?del=" . $itemResult['menu_itemID'] . "' title='Delete Record'><span class='btn btn-danger'>Delete</span></a>";
                            echo "</td>";
                            echo "</tr>";
                        }
                    }
                    echo "</tbody>";
                    echo "</table>";
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php';
if ($checkOnDelete) {
    echo '<script> swal({
      title: "Successfully Deleted !",
      buttons: {ok: "OK"},
      })    
     </script>';
}
?>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>

<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

</body>
</html>
