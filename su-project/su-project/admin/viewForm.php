<?php
session_start();
require_once "../Global-Variables/global.php";
require_once $config;
require_once "../classes/form/form.php";


// check session
if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
}
//get data of the selected item
if (isset($_REQUEST['id'])) {
    $uid = $_REQUEST['id'];
    $form = new form();
    $formResult = $form->edit($uid);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="style/css/sb-admin.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>
<body id="page-top">
<?php
include 'header.php';
?>
<div id="content-wrapper">
    <div class="container-fluid">
        <!--title (path)-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Submission</li>
        </ol>
    </div>
    <div class="wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Submission Details</h2>
                    </div>
                    <!--form start-->
                    <form action="" method="post">
                        <div class="form-group">
                            <label>Full Name</label>
                            <input type="text" class="form-control" name="fullName"
                                   value="<?php echo $formResult['formFullName']; ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label>Mobile</label>
                            <input type="text" class="form-control" name="mobile"
                                   value="<?php echo $formResult['formMobile']; ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email"
                                   value="<?php echo $formResult['formEmail']; ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label>Applying Date</label>
                            <input type="date" id="date" name="date"
                                   value="<?php echo $formResult['formCreatedDate']; ?>"
                                   class="form-control" disabled>
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea type="text" class="form-control" name="email"
                                      disabled><?php echo $formResult['formMessage']; ?></textarea>
                        </div>
                        <a href='Form.php?'><span class='btn btn-primary'>Back</span></a>
                    </form>
                    <!--form end-->
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php';
?>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>

<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

</body>
</html>
