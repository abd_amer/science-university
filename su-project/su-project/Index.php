<?php
include 'Config/DB_conn.php';
include 'classes/events/events.php';
include 'classes/menu/menuItem.php';
include 'classes/menu/menuType.php';
include 'classes/programs/programs.php';
include 'classes/announcement/announcement.php';
include 'classes/slide/slide.php';
include 'classes/news/news.php';
include 'Global-Variables/global.php';
include 'classes/reed-for-home-page/get.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Sciences University</title>

    <!-- CSS for the website-->
    <link href="style/style.css" rel="stylesheet" type="text/css">

    <!-- Link For Icons-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- bootstrap 4 -->
    <link href="style/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">

    <!--fonts-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap"
          rel="stylesheet">

</head>
<body>
<!--header starts-->
<header>
    <?php include 'header.php'; ?>
</header>
<!--header end-->
<!--body starts-->
<div class="content">
    <!--slider starts-->
    <div class="container-fluid">
        <div id="carouselExampleIndicators" class="carousel slide" data-interval="7000" data-ride="carousel">
            <?php
            $get = new get();
            $slide = new slide();
            $resultsInner = $get->readSlide();
            $resultsIndicators = $get->readSlide();
            $minimumID = $get->selectActiveSlide();
            print "<ol class=\"carousel-indicators\">\n";
            $counter = 0;
            foreach ($resultsIndicators as $resultIndicator) {
                if ($minimumID == $resultIndicator['slideOrder']) {
                    $active = 'active';
                } else {
                    $active = null;
                }
                print "<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"$counter\" class=\"$active\"></li>\n";
                $counter++;
            }
            print "</ol>\n";
            print "<div class=\"carousel-inner\">\n";

            foreach ($resultsInner as $resultInner) {
                if ($minimumID == $resultInner['slideOrder']) {
                    $order = 'active';
                } else {
                    $order = null;
                }
                $retrieve = $slide->retrieve_Img($resultInner['slideImg']);
                $imageSrc = $imagePath . $retrieve['imageSrc'];
                $imageAlt = $retrieve['imageAlt'];
                $slideText = $resultInner['slideText'];
                print "<div class=\"carousel-item $order\">\n";
                print "    <img class=\"d-block slide-image w-100\" src=\"$imageSrc\" alt=\"$imageAlt\">\n";
                print "    <div class=\"box-on-slider\">\n";
                print "        <p class=\"long-text\">$slideText</p>\n";
                print "    </div>\n";
                print "    <div class=\"box-on-slider2\">\n";
                print "        <p>$slideText</p>\n";
                print "    </div>";
                print "</div>";

            }
            print "</div>";
            ?>
        </div>
    </div>
    <!--slider end-->
    <!--programs and news starts-->
    <div class="container">
        <div class="newRow">
            <div class="row">
                <div class="col-xl-6 col-sm-12">
                    <div class="row">
                        <?php
                        $program = new programs();
                        $programResults = $get->readProgram();
                        foreach ($programResults as $programResult) {
                            $retrieve = $program->retrieve_Img($programResult['programImage']);
                            $imageSrc = $imagePath . $retrieve['imageSrc'];
                            $imageAlt = $retrieve['imageAlt'];
                            $title = $programResult['genric_pageTitle'];
                            if (strlen($title) > 22) {
                                $title = substr($title, 0, 22);
                                $title = $title . '...';
                            }
                            $programID = $programResult['programID'];
                            print "\n";
                            print "    <div class=\"col-md-6 col-sm-12 center\">";
                            print "        <div class=\"content-wrapper \">";
                            print "            <div class=\"img-hover-zoom img-hover-zoom--xyz\">\n";
                            print "                <a href=\"program.php?programID=$programID\"><img src=\"$imageSrc \" alt=\"$imageAlt \"></a>";
                            print "            </div>\n";
                            print "        <a class=\"caption-for-top-photos\" href=\"program.php?programID=$programID\">$title</a>\n";
                            print "        </div>";
                            print "    </div>";
                            print "";
                        }
                        ?>
                    </div>
                </div>
                <div class="col-xl-6 col-sm-12">
                    <div class="col-with-text">
                        <p class="news"><a class="news" href="news.php"><strong>NEWS</strong></a></p>
                        <?php
                        $counter = 1;
                        $news = new get();
                        $newsResults = $get->readNews();
                        if (is_array($newsResults)) {
                            foreach ($newsResults as $newsResult) {
                                if ($counter == 3) {
                                    $class = null;
                                    $line = null;
                                } else {

                                    $class = 'line';
                                    $line = "<hr>";
                                }
                                $counter++;
                                $date = $newsResult['DATE_FORMAT(genric_pageDate,"%M %e %Y")'];
                                $title = $newsResult['genric_pageTitle'];
                                $body = $newsResult['genric_pageBody'];
                                $body = strip_tags($body);
                                $len = str_word_count($body);
                                $body = substr($body, 0, 120);
                                $body = $body . '...';
                                $id = $newsResult['genric_pageID'];
                                print "<div>\n";
                                print "\n";
                                print   "<p class=\"date\">$date</p>\n";
                                print   "<p class=\"para-title\"><a href=\"newsPage.php?newsID=$id\">$title</a>\n";
                                print   "<p>\n";
                                print   "<p class=\"content\">$body</p>";
                                print   "<p class=\"read-more\"><a href=\"newsPage.php?newsID=$id\">READ MORE</a></p>\n";
                                print   "<br>\n";
                                print   "<p class=\"$class\"></p>\n";
                                print   "$line";
                                print "</div>";
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--photos with news end-->
    <!--statistics starts-->
    <div class="container-fluid">
        <div class="statistics" id="counter">
            <div class="container ">
                <div class="row">
                    <div class="col-sm worker">
                        <div class="icon">
                            <img src="img/group-11.png">
                        </div>
                        <span class="numbers counter-value" data-count="90">0
                        </span><span class="captions symbol">+</span>
                        <div class="captions">
                            Profession-ready degree programs
                        </div>
                    </div>
                    <div class="col-sm salary">
                        <div class="icon">
                            <img src="img/group-12.png">
                        </div>
                        <span class="numbers counter-value" data-count="1">100
                        </span><span class="captions symbol">#</span>
                        <div class="captions">
                            Our MBA for salary-to-debt ratio
                        </div>
                    </div>
                    <div class="col-sm globally">
                        <div class="icon">
                            <img src="img/noun-64173-cc.png">
                        </div>
                        <span class="numbers counter-value" data-count="100000">0
                        </span><span class="captions symbol"></span>
                        <div class="captions">
                            Sciences University alumni worldwide
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--statistics end-->
    <!--event starts-->
    <div class="container">
        <div class="events"><a class="events" href="events.php"> EVENTS</a></div>
        <div class="row">
            <?php
            $event = new get();
            $eventResults = $event->readEvent();
            if (is_array($eventResults)) {
                foreach ($eventResults as $eventResult) {
                    $id = $eventResult['eventID'];
                    $imagEvent = new events();
                    $retrieve = $imagEvent->retrieve_Img($eventResult['eventImage']);
                    $imageSrc = $imagePath . $retrieve['imageSrc'];
                    $imageAlt = $retrieve['imageAlt'];
                    $title = $eventResult['genric_pageTitle'];
                    $campus = $eventResult['eventCampus'];
                    $body = $eventResult['genric_pageBody'];
                    $body = substr($body, 0, 120);
                    $body = strip_tags($body);
                    $body = $body . '...';
                    $date = $eventResult['DATE_FORMAT(event.eventDate,"%M %e %Y")'];
                    $startTime = $eventResult['eventStartTime'];
                    $startTime = date('h:i a', strtotime($startTime));
                    $endTime = $eventResult['eventEndTime'];
                    $endTime = date('h:i a', strtotime($endTime));
                    $fullTime = $startTime . ' ' . '-' . ' ' . $endTime;
                    $month = substr($date, 0, stripos($date, ' '));
                    $monLength = strlen($month);
                    $day = substr($date, $monLength, 3);
                    print "            <div class=\"col-xl-4 cards\">\n";
                    print "                <div class=\"card-wraper  h-100 \">\n";
                    print "                    <div class=\"card  h-100\">\n";
                    print "                        <img class=\"card-img-top\" src=\"$imageSrc\" alt=\"$imageAlt\">\n";
                    print "                        <div class=\"calender\">\n";
                    print "                            <img class=\"cal\" src=\"img/shape-copy.png\">\n";
                    print "                            <p class=\"cal-day\">$day</p>\n";
                    print "                            <p class=\"cal-month\">$month</p>\n";
                    print "                        </div>\n";
                    print "                        <div class=\"card-body h-100\">\n";
                    print "                            <p class=\"date\"><span class=\"padding-15\">$fullTime</span><span class=\"vl\"></span>\n";
                    print "                                <span>$campus</span></p>\n";
                    print "                            <p class=\"para-title \"><a href=\"eventPage.php?eventID=$id\">$title</a>\n";
                    print "                            </p>\n";
                    print "                            <p class=\"content  \">$body</p>\n";
                    print "                            <p class=\"read-more \"><a href=\"eventPage.php?eventID=$id\">LEARN MORE</a></p>\n";
                    print "                        </div>\n";
                    print "                    </div>\n";
                    print "                </div>\n";
                    print "            </div>";
                }
            }
            ?>
        </div>
    </div>
    <!--event end-->
    <!--apply now starts-->
    <div class="container-fluid">
        <div class="apply">
            <?php
            $announcement = new get();
            $announcementResult = $announcement->readAnnouncement();
            ?>
            <div class="text-btn">
                <div class="bg-edge2edge">
                    <div class="container">
                        <div class="content-wrapper clearfix">
                            <div class="apply-text"><p
                                        id="textLength"><?php echo $announcementResult['announcementText'] ?></p>
                            </div>
                            <div class="apply-now"><input
                                        onclick="window.location.href = '<?php echo $announcementResult["announcementActionLink"] ?>';"
                                        value="<?php echo $announcementResult["announcementActionTitle"] ?>"
                                        class="button-apply" type="button">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--apply now end-->
    <!--form starts-->

    <div class="container">
        <div class="formBox">
            <form class="form-content" method="post" action="" id="form1">
                <div class="row">
                    <div class="col-md-12 touch ">
                        <h5 class="appear"><b>Submitted Successfully</b></h5>
                        <a href="contact-us.php"><h1>GET IN TOUCH</h1></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="inputBox ">
                            <input type="text" name="fullName" placeholder="Full Name" class="input"
                                   minlength="5" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="inputBox mobile">
                            <input type="tel" name="mobile" placeholder="Mobile" id="tel" class="input" required
                                   minlength="5">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="inputBox email">
                            <input name="email" placeholder="Email" class="input" type="email"
                                   pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="inputBox">
                            <textarea name="message" class="input" placeholder="Message"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 btn">
                        <input type="submit" name="submitForm" value="SUBMIT" class="button subbtn two ">
                    </div>
                </div>
            </form>
            <?php
            if (isset($_POST['submitForm'])) {
                $name = $_POST['fullName'];
                $mobile = $_POST['mobile'];
                $email = $_POST['email'];
                $message = $_POST['message'];
                $contact = new get();
                $insert = $contact->insertContactUs($name, $mobile, $email, $message);

                echo '<script> swal({
                   title: "Successfully Submitted !",
                    buttons: {ok: "OK"},
                    })    
                </script>';
            }
            ?>
        </div>
    </div>
    <!--form end-->
</div>
<!--body end-->
<!--footer start-->
<footer>
    <?php include 'footer.php'; ?>
</footer>
<!--footer end-->
<!-- JQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!--java script for the website-->
<script src="JS/JS-for-index.js"></script>
</body>
</html>