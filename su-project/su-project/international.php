<?php
include 'Config/DB_conn.php';
include 'classes/menuPages/menuPages.php';
include 'Global-Variables/global.php';
include 'classes/reed-for-home-page/get.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Sciences University</title>

    <link href="style//style.css" rel="stylesheet" type="text/css">

    <link href="style/styleinner.css" rel="stylesheet" type="text/css">

    <link href='https://fonts.googleapis.com/css?family=Roboto:500,900,100,300,700,400' rel='stylesheet'
          type='text/css'>

    <!-- Link For Icons-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- bootstrap 4 -->
    <link href="style/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- JQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="http://code.jquery.com/jquery.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap"
          rel="stylesheet">
</head>
<body>
<header>
    <?php include 'header.php'; ?>
</header>
<div class="body">
    <?php
    $internationalImage = new menuPages();
    $international = new get();
    $result = $international->readInternational();
    $retrieve = $internationalImage->retrieve_Img($result['genric_pageHeaderImage']);
    $imageSrc = $imagePath . $retrieve['imageSrc'];
    $imageAlt = $retrieve['imageAlt'];
    ?>
    <div>
        <img class="banner img-fluid" src="<?php echo $imageSrc ?>" alt="<?php echo $imageAlt ?>">
    </div>
    <div class="container">
        <div class="menu-item">
            <div class="title-au">
                <p class="news" style="text-align: center"><?php echo $result['genric_pageTitle'] ?></p>
            </div>
            <div class="content-lp">
                <p class="content"><?php echo $result['genric_pageBody'] ?></p>
            </div>
        </div>
    </div>
</div>
<footer>
    <?php include 'footer.php'; ?>
</footer>
<script src="JS/innerjs.js"></script>
</body>
</html>