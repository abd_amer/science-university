<?php


class events extends DB_conn
{
    //get event data
    public function read()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select event.eventID,event.eventDate,genric_page.genric_pageTitle 
        from event inner join genric_page on event.eventPageID=genric_page.genric_pageID where 
        genric_page.genric_pageStatus!=0 and event.eventStatus!=0";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //delete event
    public function delete($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "UPDATE genric_page inner join event on event.eventPageID=genric_page.genric_pageID set genric_pageStatus=0 , eventStatus=0 WHERE eventID = :id";
        $result = $connection->prepare($sql);
        $result->bindValue(':id', $id);
        $check = $result->execute();
        return $check;
    }

    //insert event data, event image, header image
    public function insert($date, $title, $body, $author, $imageAlt, $imageSrc, $imageSrc2, $imageAlt2, $startTime, $endTime, $campus)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $insertEventImageSql = "INSERT INTO image (imageAlt,imageSrc,imageModificationTime) VALUES('$imageAlt','$imageSrc',CURRENT_DATE )";
        $this->connect()->query($insertEventImageSql);

        $getImageSql = "select max(imageID) as max_id from image";
        $getImageResult = $connection->query($getImageSql);
        $imageRow = $getImageResult->fetch(PDO::FETCH_ASSOC);
        $maxImg_id = $imageRow['max_id'];

        $insertPageSql = "INSERT INTO genric_page (genric_pageTitle,genric_pageBody,
        genric_pageModificationDate,genric_pageAutherID,genric_pageStatus,genric_pageDate,genric_pageType,genric_pageHeaderImage)
        values ('$title','$body',CURRENT_DATE ,'$author',1,null,'event',$maxImg_id)";
        $connection->query($insertPageSql);

        $getPageID = "select max(genric_pageID) as pageID from genric_page";
        $getPageResult = $connection->query($getPageID);
        $maxPageID = $getPageResult->fetch(PDO::FETCH_ASSOC);
        $pageID = $maxPageID['pageID'];

        $insertHeaderImageSql = "INSERT INTO image (imageAlt,imageSrc,imageModificationTime) VALUES('$imageAlt2','$imageSrc2',CURRENT_DATE )";
        $this->connect()->query($insertHeaderImageSql);

        $getHeaderImageSql = "select max(imageID) as maxPage from image";
        $getHeaderImageResult = $connection->query($getHeaderImageSql);

        $maxHeaderImage = $getHeaderImageResult->fetch(PDO::FETCH_ASSOC);
        $maxHeaderImageID = $maxHeaderImage['maxPage'];

        $insertEventSql = "INSERT INTO event (eventDate,eventImage,
        eventStartTime,eventEndTime,eventStatus,eventPageID,eventCampus)
        values ('$date',$maxHeaderImageID,'$startTime','$endTime',1,$pageID,'$campus')";
        $eventResult = $connection->query($insertEventSql);
        return $eventResult;
    }

    //save image into image table
    public function saveImage($imageAlt, $imageSrc)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $insertSql = "INSERT INTO image (imageAlt,imageSrc,imageModificationTime) VALUES('$imageAlt','$imageSrc',CURRENT_DATE )";
        $insertResult = $connection->prepare($insertSql);
        $insertResult->execute();
        $getLastID = "select max(imageID) as max_id from image";
        $getResult = $connection->prepare($getLastID);
        $getResult->execute();
        $maxImage = $getResult->fetch(PDO::FETCH_ASSOC);
        $max_id = $maxImage['max_id'];
        return $max_id;
    }

    //get current data of the event page to edit
    public function editPage($id)
    {
        $sql = "select genric_pageTitle,genric_pageBody,genric_pageDate,genric_pageHeaderImage from genric_page where genric_pageID = :id";
        $result = $this->connect()->prepare($sql);
        $result->bindValue(":id", $id);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    //get current data of the event  to edit
    public function editEvent($id)
    {
        $sql = "select * from event where eventID = :id";
        $result = $this->connect()->prepare($sql);
        $result->bindValue(":id", $id);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    //get event image
    public function retrieve_Img($imgID)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "SELECT imageSrc,imageAlt FROM image WHERE imageID = '$imgID'";
        $result = $connection->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        if ($row != null) {
            $result->execute();
        }
        return $row;
    }

    //update event and event page data individually
    public function update($fields, $eventFields, $id, $id2)
    {
        $pageField = "";
        $counter = 1;
        $total_fields = count($fields);
        foreach ($fields as $key => $value) {
            if ($counter === $total_fields) {
                $set = "$key = :" . $key;
                $pageField = $pageField . $set;
            } else {
                $set = "$key = :" . $key . ", ";
                $pageField = $pageField . $set;
                $counter++;
            }
        }
        $sql = "";
        $sql .= "update genric_page set " . $pageField;
        $sql .= " where genric_pageID = " . $id;
        $stmt = $this->connect()->prepare($sql);
        foreach ($fields as $key => $value) {
            $stmt->bindValue(':' . $key, $value);
        }
        $stmt->execute();

        $eventField = "";
        $counter = 1;
        $total_fields = count($eventFields);
        foreach ($eventFields as $key => $value) {
            if ($counter === $total_fields) {
                $set = "$key = :" . $key;
                $eventField = $eventField . $set;
            } else {
                $set = "$key = :" . $key . ", ";
                $eventField = $eventField . $set;
                $counter++;
            }
        }
        $sql = "";
        $sql .= "update event set " . $eventField;
        $sql .= " where eventID = " . $id2;
        $stmt = $this->connect()->prepare($sql);
        foreach ($eventFields as $key => $value) {
            $stmt->bindValue(':' . $key, $value);
        }
        $check = $stmt->execute();
        return $check;
    }
}