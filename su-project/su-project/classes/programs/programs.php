<?php


class programs extends DB_conn
{
    //get all programs
    public function read()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select program.programID,genric_page.genric_pageTitle ,program.programParentID
        from program inner join genric_page on program.programPageID=genric_page.genric_pageID where 
        genric_page.genric_pageStatus!=0 and program.programStatus!=0 ";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //delete selected program
    public function delete($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "UPDATE genric_page inner join program on program.programPageID=genric_page.genric_pageID set genric_pageStatus=0 , programStatus=0 WHERE programID = :id";
        $result = $connection->prepare($sql);
        $result->bindValue(':id', $id);
        $check = $result->execute();
        return $check;
    }

    //add new program
    public function insert($title, $body, $author, $HeaderImageAlt, $HeaderImageSrc, $programImageSrc, $programImageAlt, $parentID)
    {

        $db = new DB_conn();
        $connection = $db->connect();
        $insertImageSql = "INSERT INTO image (imageAlt,imageSrc,imageModificationTime) VALUES('$HeaderImageAlt','$HeaderImageSrc',CURRENT_DATE )";
        $this->connect()->query($insertImageSql);

        $getMaxImageSql = "select max(imageID) as max_id from image";
        $imageResult = $connection->query($getMaxImageSql);
        $maxImage = $imageResult->fetch(PDO::FETCH_ASSOC);
        $maxImg_id = $maxImage['max_id'];

        $insertPageSql = "INSERT INTO genric_page (genric_pageTitle,genric_pageBody,
        genric_pageModificationDate,genric_pageAutherID,genric_pageStatus,genric_pageDate,genric_pageType,genric_pageHeaderImage)
        values ('$title','$body',CURRENT_DATE ,'$author',1,null,'program',$maxImg_id)";
        $connection->query($insertPageSql);

        $getMaxPageSql = "select max(genric_pageID) as pageID from genric_page";
        $pageResult = $connection->query($getMaxPageSql);
        $maxPage = $pageResult->fetch(PDO::FETCH_ASSOC);
        $pageID = $maxPage['pageID'];

        $insertHeaderImageSql = "INSERT INTO image (imageAlt,imageSrc,imageModificationTime) VALUES('$programImageAlt','$programImageSrc',CURRENT_DATE )";
        $this->connect()->query($insertHeaderImageSql);

        $getHeaderImageSql = "select max(imageID) as max_id2 from image";
        $headerImageResult = $connection->query($getHeaderImageSql);
        $headerImage = $headerImageResult->fetch(PDO::FETCH_ASSOC);
        $maxHeaderImg = $headerImage['max_id2'];

        $insertProgramSql = "INSERT INTO program (programParentID,programImage,programPageID,programModificationDate)
        values ($parentID,$maxHeaderImg,$pageID,CURRENT_DATE )";
        $check = $connection->query($insertProgramSql);
        return $check;
    }

    //save image into image table and return the id of it
    public function saveImage($imageAlt, $imageSrc)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $insertSql = "INSERT INTO image (imageAlt,imageSrc,imageModificationTime) VALUES('$imageAlt','$imageSrc',CURRENT_DATE )";
        $insertResult = $connection->prepare($insertSql);
        $insertResult->execute();
        $getMaxSql = "select max(imageID) as max_id from image";
        $getResult = $connection->prepare($getMaxSql);
        $getResult->execute();
        $max = $getResult->fetch(PDO::FETCH_ASSOC);
        $max_id = $max['max_id'];
        return $max_id;
    }

    //get current program page data to edit
    public function editPage($id)
    {
        $sql = "select genric_pageTitle,genric_pageBody,genric_pageDate,genric_pageHeaderImage from genric_page where genric_pageID = :id";
        $result = $this->connect()->prepare($sql);
        $result->bindValue(":id", $id);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    //get current program data to edit
    public function editProgram($id)
    {
        $sql = "select * from program where programID = :id";
        $result = $this->connect()->prepare($sql);
        $result->bindValue(":id", $id);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    //get parents
    public function selectParent($id)
    {
        $sql = "select * from program inner join genric_page on programPageID = genric_pageID where programID = :id";
        $result = $this->connect()->prepare($sql);
        $result->bindValue(":id", $id);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    //get program header image
    public function retrieve_Img($imgID)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "SELECT imageSrc,imageAlt FROM image WHERE imageID = '$imgID'";
        $result = $connection->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        if ($row != null) {
            $result->execute();
        }
        return $row;
    }

    //update selected program
    public function update($fields, $id)
    {
        $st = "";
        $counter = 1;
        $total_fields = count($fields);
        foreach ($fields as $key => $value) {
            if ($counter === $total_fields) {
                $set = "$key = :" . $key;
                $st = $st . $set;
            } else {
                $set = "$key = :" . $key . ", ";
                $st = $st . $set;
                $counter++;
            }
        }
        $sql = "";
        $sql .= "update genric_page set " . $st;
        $sql .= " where genric_pageID = " . $id;
        $stmt = $this->connect()->prepare($sql);
        foreach ($fields as $key => $value) {
            $stmt->bindValue(':' . $key, $value);
        }
        $check = $stmt->execute();
        return $check;
    }

    //update program image
    public function updateProgram($programImage, $id)
    {
        $sql = "update program set programImage = $programImage where programID = $id";
        $result = $this->connect()->prepare($sql);
        $check = $result->execute();
        return $check;
    }
}