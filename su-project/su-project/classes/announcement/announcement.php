<?php

class announcement extends DB_conn
{
    //get announcement data
    public function read()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select * from announcement where announcementStatus !=0";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //delete announcement
    public function delete($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "UPDATE announcement set announcementStatus=0 WHERE announcementID = :id";
        $result = $connection->prepare($sql);
        $result->bindValue(':id', $id);
        $check = $result->execute();
        return $check;
    }

    //save image into image table for announcement
    public function saveImage($imageAlt, $imageSrc)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $insertImageSql = "INSERT INTO image (imageAlt,imageSrc,imageModificationTime) VALUES('$imageAlt','$imageSrc',CURRENT_DATE )";
        $insertResult = $connection->prepare($insertImageSql);
        $insertResult->execute();
        $getImageSql = "select max(imageID) as max_id from image";
        $getResult = $connection->prepare($getImageSql);
        $getResult->execute();
        $row = $getResult->fetch(PDO::FETCH_ASSOC);
        $max_id = $row['max_id'];
        return $max_id;
    }

    //get current data of the item to edit
    public function edit($id)
    {
        $sql = "select * from announcement where announcementID = :id";
        $result = $this->connect()->prepare($sql);
        $result->bindValue(":id", $id);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    //get image of the announcement item
    public function retrieve_Img($imgID)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "SELECT imageSrc,imageAlt FROM image WHERE imageID = '$imgID'";
        $result = $connection->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        if ($row != null) {
            $result->execute();
        }
        return $row;
    }

    //update announcement
    public function update($fields, $id)
    {
        $field = "";
        $counter = 1;
        $total_fields = count($fields);
        foreach ($fields as $key => $value) {
            if ($counter === $total_fields) {
                $set = "$key = :" . $key;
                $field = $field . $set;
            } else {
                $set = "$key = :" . $key . ", ";
                $field = $field . $set;
                $counter++;
            }
        }
        $sql = "";

        $sql .= "update announcement set " . $field;
        $sql .= " where announcementID = " . $id;

        $result = $this->connect()->prepare($sql);
        foreach ($fields as $key => $value) {
            $result->bindValue(':' . $key, $value);
        }
        $check = $result->execute();
        return $check;
    }
}