<?php


class user extends DB_conn
{
    //get all users
    public function read()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select userID,userRole,userName,userFullName,userModificationTime from user where userStatus !=0";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //delete selected user
    public function delete($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "UPDATE user set userStatus=0 WHERE userID = :id";
        $result = $connection->prepare($sql);
        $result->bindValue(':id', $id);
        $check = $result->execute();
        return $check;
    }

    //generating salt for encryption

    public function register($userName, $userFullName, $userRole, $password)
    {
        $salt = $this->generateSalt();
        $hash = password_hash($password, PASSWORD_DEFAULT, [$salt]);
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "INSERT INTO user (userName,userFullName,
        userModificationTime,userRole,userPassword,userSalt)
        values ('$userName','$userFullName',CURRENT_DATE ,'$userRole','$hash','$salt')";
        $result = $connection->prepare($sql);
        if ($result->execute()) {
            return true;
        }
    }

    //add new user with hashed password

    private function generateSalt($length = 16)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $salt = '';
        for ($i = 0; $i < $length; $i++) {
            $salt .= $characters[rand(0, $charactersLength - 1)];
        }
        return $salt;
    }

}