<?php

class get extends DB_conn
{
    //get all slide
    public function readSlide()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select * from slide where slideStatus !=0 order by slideOrder";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //select the active slide
    public function selectActiveSlide()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select min(slideOrder) as minID from slide where slideStatus!=0";
        $result = $connection->query($sql);
        $row = $result->fetch();
        $id = $row['minID'];
        return $id;
    }

    //get all programs
    public function readProgram()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select * from program inner join genric_page on program.programPageID = genric_page.genric_pageID
                                where programStatus !=0 and programParentID is null";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //get last 3 news order by date
    public function readNews()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select genric_pageID,genric_pageBody , genric_pageTitle , DATE_FORMAT(genric_pageDate,\"%M %e %Y\") 
                from genric_page where genric_pageStatus!=0 and genric_pageType='news' order by  genric_pageDate DESC limit 3";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //get last 3 events order by date
    public function readEvent()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select event.eventID , event.eventCampus , event.eventStartTime , event.eventEndTime , event.eventImage, 
                DATE_FORMAT(event.eventDate,\"%M %e %Y\"), genric_page.genric_pageTitle , genric_page.genric_pageBody 
                from event inner join genric_page on event.eventPageID=genric_page.genric_pageID where 
                genric_page.genric_pageStatus!=0 and event.eventStatus!=0 order by eventDate limit 3";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //get announcement data
    public function readAnnouncement()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select * from announcement where announcementID = 1";
        $result = $connection->query($sql);
        $row = $result->fetch();
        return $row;
    }

    //insert contact us data to database
    public function insertContactUs($name, $mobile, $email, $message)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "insert into contact_us (formFullName ,formMobile,formEmail,formMessage,formCreatedDate) 
                    values ('$name','$mobile','$email','$message',CURRENT_DATE )";
        $result = $connection->prepare($sql);
        $result->execute();
    }

    //get about us page
    public function readAboutUs()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select genric_pageHeaderImage,genric_pageTitle , genric_pageBody from genric_page where genric_pageType='about'";
        $result = $connection->query($sql);
        $row = $result->fetch();
        return $row;
    }

    //get academics page
    public function readAcademics()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select genric_pageHeaderImage,genric_pageTitle , genric_pageBody from genric_page where genric_pageType='academics'";
        $result = $connection->query($sql);
        $row = $result->fetch();
        return $row;
    }

    //get admission page
    public function readAdmission()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select genric_pageHeaderImage,genric_pageTitle , genric_pageBody from genric_page where genric_pageType='admissions'";
        $result = $connection->query($sql);
        $row = $result->fetch();
        return $row;
    }

    //get international page
    public function readInternational()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select genric_pageHeaderImage,genric_pageTitle , genric_pageBody from genric_page where genric_pageType='international'";
        $result = $connection->query($sql);
        $row = $result->fetch();
        return $row;
    }

    //get selected event page
    public function readSelectedEvent($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select event.eventID,genric_page.genric_pageHeaderImage , event.eventCampus , event.eventStartTime , event.eventEndTime , event.eventImage, 
                    DATE_FORMAT(event.eventDate,\"%M %e %Y\"), genric_page.genric_pageTitle , genric_page.genric_pageBody 
                    from event inner join genric_page on event.eventPageID=genric_page.genric_pageID where 
                    event.eventID=$id";
        $result = $connection->query($sql);
        $row = $result->fetch();
        return $row;
    }

    //get all event except the selected one
    public function readEventsWithoutTheSelected($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select event.eventID , event.eventCampus , event.eventStartTime , event.eventEndTime , event.eventImage, 
                    DATE_FORMAT(event.eventDate,\"%M %e %Y\"), genric_page.genric_pageTitle , genric_page.genric_pageBody 
                    from event inner join genric_page on event.eventPageID=genric_page.genric_pageID where 
                    genric_page.genric_pageStatus!=0 and eventID!=$id and event.eventStatus!=0 order by eventDate limit 3";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //get all event
    public function readAllEvent()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select event.eventID , event.eventCampus , event.eventStartTime , event.eventEndTime , event.eventImage, 
                DATE_FORMAT(event.eventDate,\"%M %e %Y\"), genric_page.genric_pageTitle , genric_page.genric_pageBody 
                from event inner join genric_page on event.eventPageID=genric_page.genric_pageID where 
                genric_page.genric_pageStatus!=0 and event.eventStatus!=0 order by eventDate";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //get all news
    public function readAllNews()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select genric_pageID,genric_pageBody , genric_pageTitle , DATE_FORMAT(genric_pageDate,\"%M %e %Y\") 
                       from genric_page where genric_pageStatus!=0 and genric_pageType='news' order by  genric_pageDate DESC";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //get selected news page
    public function readSelectedNews($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select genric_pageHeaderImage,genric_pageID,genric_pageBody , genric_pageTitle , DATE_FORMAT(genric_pageDate,\"%M %e %Y\") 
                       from genric_page where genric_pageID = $id";
        $result = $connection->query($sql);
        $row = $result->fetch();
        return $row;
    }

    //get selected program
    public function readSelectedProgram($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select * from program inner join genric_page on program.programPageID = genric_page.genric_pageID
                where programID = $id and programStatus!=0 and genric_pageStatus!=0";
        $result = $connection->query($sql);
        $row = $result->fetch();
        return $row;
    }

    //get program childes
    public function readChildProgram($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select * from program inner join genric_page on program.programPageID = genric_page.genric_pageID
                                where programParentID = $id and programStatus!=0 and genric_pageStatus!=0";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //get header menu
    public function readHeader()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select menu_item.menu_itemID,menu_item.menu_itemName , menu_item.menu_itemLink from 
                menu_item inner join menu on menu_item.menu_itemType = menu.menuID where menu.menuType='header' 
                and menu_itemStatus!=0 order by menu_itemOrder";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //get parent footer menu items
    public function readParentFooter()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select menu_item.menu_itemOrder,menu_item.menu_itemNewLine,menu_item.menu_itemParentID, menu_item.menu_itemID,menu_item.menu_itemName , 
                menu_item.menu_itemLink, menu.menuType from menu_item inner join 
                menu on menu_item.menu_itemType = menu.menuID where menu.menuType = 'footer' and 
                menu_itemParentID is null and menu_itemStatus!=0 order by menu_itemOrder ";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //get all childes footer menu item
    public function readChildFooter($parent)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select menu_item.menu_itemNewLine,menu_item.menu_itemParentID, menu_item.menu_itemID,menu_item.menu_itemName , 
                 menu_item.menu_itemLink, menu.menuType from menu_item inner join 
                 menu on menu_item.menu_itemType = menu.menuID where menu.menuType = 'footer' and menu_itemParentID = $parent
                 and menu_itemStatus!=0";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

}