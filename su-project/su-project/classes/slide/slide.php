<?php

class slide extends DB_conn
{
    //get all slides data
    public function read()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select slideID,slideOrder,slideText from slide where slideStatus !=0";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //delete selected slide
    public function delete($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "UPDATE slide set slideStatus=0 WHERE slideID = :id";
        $result = $connection->prepare($sql);
        $result->bindValue(':id', $id);
        $check = $result->execute();
        return $check;
    }

    //add new slide
    public function insert($text, $order, $author, $imageAlt, $imageSrc)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $insertImageSql = "INSERT INTO image (imageAlt,imageSrc,imageModificationTime) VALUES('$imageAlt','$imageSrc',CURRENT_DATE )";
        $this->connect()->query($insertImageSql);
        $getMaxSql = "select max(imageID) as max_id from image";
        $getMaxResult = $connection->query($getMaxSql);
        $max = $getMaxResult->fetch(PDO::FETCH_ASSOC);
        $max_id = $max['max_id'];
        $insertSlideSql = "INSERT INTO slide (slideText,slideOrder,
        slideModificationTime,slideUserID,slideStatus,slideImg)
        values ('$text','$order',CURRENT_DATE ,'$author',1,$max_id)";
        $check = $connection->query($insertSlideSql);
        return $check;

    }

    //save image into image table and return the id of it
    public function saveImage($imageAlt, $imageSrc)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $insertSql = "INSERT INTO image (imageAlt,imageSrc,imageModificationTime) VALUES('$imageAlt','$imageSrc',CURRENT_DATE )";
        $insertResult = $connection->prepare($insertSql);
        $insertResult->execute();
        $getMaxSql = "select max(imageID) as max_id from image";
        $getResult = $connection->prepare($getMaxSql);
        $getResult->execute();
        $max = $getResult->fetch(PDO::FETCH_ASSOC);
        $max_id = $max['max_id'];
        return $max_id;
    }

    //get selected slide data to edit
    public function edit($id)
    {
        $sql = "select * from slide where slideID = :id";
        $result = $this->connect()->prepare($sql);
        $result->bindValue(":id", $id);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    //get news header image
    public function retrieve_Img($imgID)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "SELECT imageSrc,imageAlt FROM image WHERE imageID = '$imgID'";
        $result = $connection->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        if ($row != null) {
            $result->execute();
        }
        return $row;
    }

    //update slide
    public function update($fields, $id)
    {
        $field = "";
        $counter = 1;
        $total_fields = count($fields);
        foreach ($fields as $key => $value) {
            if ($counter === $total_fields) {
                $set = "$key = :" . $key;
                $field = $field . $set;
            } else {
                $set = "$key = :" . $key . ", ";
                $field = $field . $set;
                $counter++;
            }
        }
        $sql = "";
        $sql .= "update slide set " . $field;
        $sql .= " where slideID = " . $id;
        $stmt = $this->connect()->prepare($sql);
        foreach ($fields as $key => $value) {
            $stmt->bindValue(':' . $key, $value);
        }
        $check = $stmt->execute();
        return $check;
    }
}