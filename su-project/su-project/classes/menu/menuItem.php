<?php

class menuItem extends DB_conn
{
    //get all items data
    public function read()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select * from menu_item where menu_itemStatus!=0";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }
    //delete selected item
    public function delete($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "UPDATE menu_item set menu_itemStatus=0 WHERE menu_itemID = :id";
        $result = $connection->prepare($sql);
        $result->bindValue(':id', $id);
        $check = $result->execute();
        return $check;
    }
    //insert menu item
    public function insert($newLine, $menuItem, $linkable, $itemType, $itemPath, $author, $itemOrder, $parentID)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "INSERT INTO menu_item (menu_itemNewLine,menu_itemName,menu_itemIsLinkable,
        menu_itemType,menu_itemLink,menu_itemAutherID,menu_itemOrder ,menu_itemParentID
        ,menu_itemModificationDate) values ($newLine,'$menuItem',$linkable,$itemType,'$itemPath','$author'
        ,$itemOrder,$parentID,CURRENT_DATE )";
        $result = $connection->query($sql);
        return $result;

    }
    //get the type of the items
    public function selectType()
    {
        $sql = "select menuID,menuType from menu where menuStatus!=0";
        $result = $this->connect()->prepare($sql);
        $result->execute();
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }
    //get type of selected item
    public function itemType($id)
    {
        $sql = "select * from menu_item inner join menu on menu_item.menu_itemType = menu.menuID where menu_itemID = :id";
        $result = $this->connect()->prepare($sql);
        $result->bindValue(":id", $id);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }
    //get current item data to edit
    public function editItem($id)
    {
        $sql = "select * from menu_item where menu_itemID = :id";
        $result = $this->connect()->prepare($sql);
        $result->bindValue(":id", $id);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }
    //get current item type to edit
    public function editType($id)
    {
        $sql = "select * from menu where menuID = :id";
        $result = $this->connect()->prepare($sql);
        $result->bindValue(":id", $id);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }
    public function getChildes($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select menu_itemName from menu_item inner join menu on menu_item.menu_itemType = menu.menuID where menu_itemType = $id and menu_itemStatus!=0 and menuStatus!=0";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }
    //update item
    public function update($fields, $id)
    {
        $field = "";
        $counter = 1;
        $total_fields = count($fields);
        foreach ($fields as $key => $value) {
            if ($counter === $total_fields) {
                $set = "$key = :" . $key;
                $field = $field . $set;
            } else {
                $set = "$key = :" . $key . ", ";
                $field = $field . $set;
                $counter++;
            }
        }
        $sql = "";
        $sql .= "update menu_item set " . $field;
        $sql .= " where menu_itemID = " . $id;
        $stmt = $this->connect()->prepare($sql);
        foreach ($fields as $key => $value) {
            $stmt->bindValue(':' . $key, $value);
        }
        $check = $stmt->execute();
        return $check;
    }
    //update item parent
    public function updateParentID($parent, $id)
    {
        $sql = "update menu_item set menu_itemParentID = $parent where menu_itemID = $id";
        $result = $this->connect()->prepare($sql);
        $result->execute();
    }
    //update item type
    public function menuItemTypeAndNewLine($newLine, $menuItemType, $id ,$menu_itemIsLinkable)
    {
        $sql = "update menu_item set menu_itemType = $menuItemType , menu_itemNewLine=$newLine , menu_itemIsLinkable = $menu_itemIsLinkable where menu_itemID = $id";
        $result = $this->connect()->prepare($sql);
        $result->execute();
    }
}