<?php


class menuType extends DB_conn
{
    //get all records data
    public function read()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select * from menu where menuStatus!=0";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //delete selected record
    public function delete($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "UPDATE menu set menuStatus=0 WHERE menuID = :id";
        $result = $connection->prepare($sql);
        $result->bindValue(':id', $id);
        $check = $result->execute();
        return $check;
    }

    //add new type
    public function insert($menuType, $authorID)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "INSERT INTO menu (menuType,menuAutherID) values ('$menuType','$authorID')";
        $result = $connection->query($sql);
        return $result;
    }

    //get selected item data
    public function edit($id)
    {
        $sql = "select * from menu where menuID = :id";
        $result = $this->connect()->prepare($sql);
        $result->bindValue(":id", $id);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    //update selected item
    public function update($menuType, $id)
    {
        $sql = "update menu set menuType = '$menuType' where menuID = $id";
        $result = $this->connect()->prepare($sql);
        $check = $result->execute();
        return $check;

    }

    //get menu item type
    public function items($id)
    {
        $sql = "select menu_itemName from menu_item where menu_itemType = $id";
        $result = $this->connect()->prepare($sql);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

}