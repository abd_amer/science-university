<?php

//include ('../Config/DB_conn.php');

session_start();
class User extends DB_conn
{

    public function login($username, $password)
    {

        $db = new DB_conn();
        $connection = $db->connect();
        try {
            if (isset($_POST['login'])) {
                $sql = "select userName , userPassword ,userID from user where userName = '" . $username . "' and userStatus!=0 and userRole=1";
                $statement = $connection->prepare($sql);
                $statement->execute();
                $result = $statement->fetch();
                if ($result) {
                    if ($result['userName'] == $username && $result['userPassword']) {
                        $userPassword = $result['userPassword'];
                        if (password_verify($password, $userPassword)) {
                            $_SESSION['id'] = $result['userID'];
                            return true;
                        }
                    }
                }
                return false;
            }
        } catch (PDOException $e) {
            echo "Connection failed : " . $e->getMessage();
        }
    }

    public function register($username, $password)
    {
        $salt = $this->generateSalt();
        $hash = password_hash($password, PASSWORD_DEFAULT, [$salt]);
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "UPDATE user set userPassword = '$hash' , userSalt = '$salt' where userID = 1";
        $stmt = $connection->prepare($sql);
        $stmt->execute();
    }

    private function generateSalt($length = 16)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $salt = '';
        for ($i = 0; $i < $length; $i++) {
            $salt .= $characters[rand(0, $charactersLength - 1)];
        }
        return $salt;
    }

}

