<?php


class menuPages extends DB_conn
{
    //get all pages data
    public function read()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select genric_pageID,genric_pageTitle,genric_pageType from genric_page where genric_pageType 
        = 'about' or genric_pageType='admissions' or genric_pageType='international' or genric_pageType='academics' and  genric_pageStatus !=0";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //delete menu page
    public function delete($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "UPDATE genric_page set genric_pageStatus=0 WHERE genric_pageID = :id";
        $result = $connection->prepare($sql);
        $result->bindValue(':id', $id);
        $check = $result->execute();
        return $check;
    }

    //save image into image table and return the id of it
    public function saveImage($imageAlt, $imageSrc)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $insertSql = "INSERT INTO image (imageAlt,imageSrc,imageModificationTime) VALUES('$imageAlt','$imageSrc',CURRENT_DATE )";
        $insertResult = $connection->prepare($insertSql);
        $insertResult->execute();
        $getMaxSql = "select max(imageID) as max_id from image";
        $getResult = $connection->prepare($getMaxSql);
        $getResult->execute();
        $max = $getResult->fetch(PDO::FETCH_ASSOC);
        $max_id = $max['max_id'];
        return $max_id;
    }

    //get selected item data to edit
    public function edit($id)
    {
        $sql = "select genric_pageTitle,genric_pageBody,genric_pageHeaderImage from genric_page where genric_pageID = :id";
        $result = $this->connect()->prepare($sql);
        $result->bindValue(":id", $id);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    //get menu page header image
    public function retrieve_Img($imgID)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "SELECT imageSrc,imageAlt FROM image WHERE imageID = '$imgID'";
        $result = $connection->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        if ($row != null) {
            $result->execute();
        }
        return $row;
    }

    //update menu page
    public function update($fields, $id)
    {
        $field = "";
        $counter = 1;
        $total_fields = count($fields);
        foreach ($fields as $key => $value) {
            if ($counter === $total_fields) {
                $set = "$key = :" . $key;
                $field = $field . $set;
            } else {
                $set = "$key = :" . $key . ", ";
                $field = $field . $set;
                $counter++;
            }
        }
        $sql = "";
        $sql .= "update genric_page set " . $field;
        $sql .= " where genric_pageID = " . $id;
        $stmt = $this->connect()->prepare($sql);
        foreach ($fields as $key => $value) {
            $stmt->bindValue(':' . $key, $value);
        }
        $check = $stmt->execute();
        return $check;
    }
}