<?php


class form extends DB_conn
{
    //get submission form data
    public function read()
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "select * from contact_us where formStatus !=0";
        $result = $connection->query($sql);
        if ($result->rowCount() > 0) {
            while ($row = $result->fetch()) {
                $rowArray[] = $row;
            }
            return $rowArray;
        }
    }

    //delete selected form
    public function delete($id)
    {
        $db = new DB_conn();
        $connection = $db->connect();
        $sql = "UPDATE contact_us set formStatus=0 WHERE formID = :id";
        $result = $connection->prepare($sql);
        $result->bindValue(':id', $id);
        $check = $result->execute();
        return $check;
    }

    //get selected form to show data
    public function edit($id)
    {
        $sql = "select * from contact_us where formID = :id";
        $result = $this->connect()->prepare($sql);
        $result->bindValue(":id", $id);
        $result->execute();
        $check = $result->fetch(PDO::FETCH_ASSOC);
        return $check;
    }
}