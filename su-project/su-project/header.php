<?php
?>
<!--menu with logo and social icons starts-->

<div class="navbar menu1">
    <div class="container">
        <div class="col-6">
            <a href="/Index.php"><img src="/img/group-4.png"
                                                                 class="su-logo" alt="SU Logo"></a>
        </div>
        <div class="col-6">
            <div class="social-logo">
                <ul>
                    <li><a href="https://www.linkedin.com"><i class="fab fa-linkedin-in"></i></a></li>
                    <li><a href="https://www.youtube.com"><i class="fab fa-youtube"></i></a></li>
                    <li><a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a></li>
                    <li><a href="https://www.twitter.com"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="https://www.facebook.com"><i class="fab fa-facebook-f"></i></a></li>
                    <li>
                        <form name="form1">
                            <input id="search" class="searchIcon" type="text" name="search" placeholder="Search"
                                   autocomplete="off" required>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--menu with logo and social icons end-->
<!--navigation bar starts-->
<div class="nav-wrapper">
    <div class="container-fluid">
        <div class="row">
            <nav class="navbar menu2 navbar-expand-lg nav-color navbar-dark">
                <a class="photo" href="/Index.php"><img
                            src="/img/group-19.png"
                            class="su-logo second-logo"
                            alt="SU Logo"></a>

                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <form name="form2" action="#">
                    <input class="searchIcon searchIcon2" type="text" placeholder="Search" autocomplete="off"
                           required>
                </form>

                <div class="container-fluid">
                    <div class="collapse navbar-collapse" id="collapsibleNavbar">
                        <ul class="m-auto navbar-nav nav-list">
                            <?php
                            $header = new get();
                            $results = $header->readHeader();
                            if (is_array($results)) {
                                foreach ($results as $result) {
                                    $path = $result['menu_itemLink'];
                                    $name = strtoupper($result['menu_itemName']);
                                    print "<li class=\"nav-item\">\n";
                                    print "    <a class=\"nav-link\" href=\"../$path\">$name</a>\n";
                                    print "</li>";
                                }
                            }
                            ?>
                            <div class="social-logo">
                                <ul>
                                    <li><a href="https://www.linkedin.com"><i class="fab fa-linkedin-in"></i></a>
                                    </li>
                                    <li><a href="https://www.youtube.com"><i class="fab fa-youtube"></i></a></li>
                                    <li><a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                                    </li>
                                    <li><a href="https://www.twitter.com"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="https://www.facebook.com"><i class="fab fa-facebook-f"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
<!--navigation bar end-->