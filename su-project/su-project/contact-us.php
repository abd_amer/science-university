<?php
include 'Config/DB_conn.php';
include 'classes/reed-for-home-page/get.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <title>Sciences University</title>
    <link href="style/style.css" rel="stylesheet" type="text/css">
    <link href="style/styleinner.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:500,900,100,300,700,400' rel='stylesheet' type='text/css'>
    <!-- Link For Icons-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- bootstrap 4 -->
    <link href="style/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- JQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery.js"></script>


    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap"
          rel="stylesheet">
</head>
<body>
<header>
    <?php include 'header.php'; ?>
</header>
<div class="body">
    <div class="container">
        <div class="formBox">
            <form class="form-content" method="post" action="" id="form1">
                <div class="row">
                    <div class="col-md-12 touch ">
                        <h5 class="appear"><b>Submitted Successfully</b></h5>
                        <a href="contact-us.php"><h1>GET IN TOUCH</h1></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="inputBox ">
                            <input type="text" name="fullName" placeholder="Full Name" class="input"
                                   minlength="5" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="inputBox mobile">
                            <input type="tel" name="mobile" placeholder="Mobile" id="tel" class="input" required
                                   minlength="5">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="inputBox email">
                            <input name="email" placeholder="Email" class="input" type="email"
                                   pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="inputBox">
                            <textarea name="message" class="input" placeholder="Message"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 btn">
                        <input type="submit" name="submitForm" value="SUBMIT" class="button subbtn two ">
                    </div>
                </div>
            </form>
        </div>
        <?php
        if (isset($_POST['submitForm'])) {
            $name = $_POST['fullName'];
            $mobile = $_POST['mobile'];
            $email = $_POST['email'];
            $message = $_POST['message'];
            $contact = new get();
            $insert = $contact->insertContactUs($name, $mobile, $email, $message);
            echo '<script> swal({
                   title: "Successfully Submitted !",
                    buttons: {ok: "OK"},
                    })    
                </script>';
        }
        ?>
        <div class="location-title"><p class="news">Our Location</p></div>
        <div class="map-responsive">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d846.2818031116457!2d35.85992464156851!3d31.95
            744538626152!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151b5f847d4ad987%3A0xd8af4445dc1773f2!2sVardot!5e0!3m2!1sen!2s
            jo!4v1562490269854!5m2!1sen!2sjo" width="400" height="300" frameborder="0" style="border:0"
                    allowfullscreen></iframe>
        </div>
    </div>
</div>
<footer>
    <?php include 'footer.php'; ?>
</footer>
<script src="JS/innerjs.js"></script>
</body>
</html>