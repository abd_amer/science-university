<?php
?>
<div class="container-fluid">
    <div class="footer">
        <div class="container">
            <div class="row">
                <?php
                $footerParent = new get();
                $results = $footerParent->readParentFooter();
                $counter = 1;
                if (is_array($results)) {
                    foreach ($results as $key => $result) {
                        $list = 'list' . $counter;
                        $name = $result['menu_itemName'];
                        $parent = $result['menu_itemID'];
                        if ($key >= 1 && $result['menu_itemNewLine'] == 1) {
                            print "                        </div>\n";
                            print "                    </div>\n";
                            print "                </div>";
                        }
                        if ($result['menu_itemNewLine'] == 1) {
                            print "<div class=\"col-md-3 \">\n";
                            print "                    <div class=\"row\">\n";
                            print "                        <div class=\"col-md-1 vir\">\n";
                            print "                            <div class=\"vl2\"></div>\n";
                            print "                        </div>\n";
                            print "                        <div class=\"col-md-11\">\n";
                        }
                        print "                            <h5 class=\"foot-title\" data-toggle=\"collapse\" data-target=\".$list\">  $name<span\n";
                        print "    class=\"arrow\">⌄</span></h5>\n";
                        $footer = new get();
                        $childResults = $footer->readChildFooter($parent);
                        print "<ul class=\"list $list collapse \">\n";
                        if (is_array($childResults)) {
                            foreach ($childResults as $childResult) {
                                $name = $childResult['menu_itemName'];
                                $path = $childResult['menu_itemLink'];
                                print "<li><a href=\"$path\">$name</a></li>\n";
                            }
                        }
                        print "</ul>\n";
                        $counter++;
                    }
                    if (count($results) > 0) {
                        print "                        </div>\n";
                        print "                    </div>\n";
                        print "                </div>";
                    }
                }
                ?>

                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-1 vir">
                            <div class="vl2"></div>
                        </div>
                        <div class="col-md-11">
                            <div><a class="foot-logo" href="/Index.php">
                                    <img src="/img/group-19.png" class="su-logo second-logo"
                                         alt="SU Logo">
                                </a>
                            </div>
                            <div>
                                <p class="foot-title follow">FOLLOW US</p>
                            </div>
                            <div class="social-logo footer-logo">
                                <ul>
                                    <li><a href="https://www.linkedin.com"><i class="fab fa-linkedin-in"></i></a>
                                    </li>
                                    <li><a href="https://www.youtube.com"><i class="fab fa-youtube"></i></a></li>
                                    <li><a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                                    </li>
                                    <li><a href="https://www.twitter.com"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="https://www.facebook.com"><i class="fab fa-facebook-f"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right">
        <div class="copy">© 2019 Sciences University. All Rights Reserved.</div>
    </div>
</div>